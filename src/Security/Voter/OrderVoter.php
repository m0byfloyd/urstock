<?php

namespace App\Security\Voter;

use App\Controller\Helpers\CompanyHelper;
use App\Entity\Order;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class OrderVoter extends Voter
{
    public const VIEW = 'ORDER_VIEW';

    public function __construct(
        private readonly Security $security,
        private readonly UserRepository $userRepository
    ) {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $attribute == self::VIEW
            && $subject instanceof \App\Entity\Order;
    }

    /**
     * @throws NonUniqueResultException
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        $user = $this->userRepository->findOneByEmail($user->getUserIdentifier());

        return match ($attribute) {
            self::VIEW => $this->canView($subject, $user),
            default => false,
        };
    }

    /**
     * @throws NonUniqueResultException
     */
    private function canView(Order $order, User $user): bool
    {
        if ($order->getOrderCompany() === CompanyHelper::getCurrentCompany($user, $this->userRepository)) {
            return true;
        }

        return false;
    }
}
