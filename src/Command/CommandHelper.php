<?php

namespace App\Command;

use App\Repository\UserRepository;
use Symfony\Component\Console\Question\Question;

class CommandHelper
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    public static function getEmailInteraction($input): array
    {
        $questions = array();

        if (!$input->getArgument('email')) {
            $question = new Question('Please give the email:');
            $question->setValidator(function ($email) {
                if (empty($email)) {
                    throw new \Exception('email can not be empty');
                }

                if (!$this->userRepository->findOneByEmail($email)) {
                    throw new \Exception('No user found with this email');
                }

                return $email;
            });
            $questions['email'] = $question;
        }

        return $questions;
    }
}
