<?php

namespace App\DataFixtures;

use App\Factory\CompanyFactory;
use App\Factory\CompanyLastVendorsManagerFactory;
use App\Factory\OrderFactory;
use App\Factory\ProductCategoryFactory;
use App\Factory\ProductFactory;
use App\Factory\ProductInOrderFactory;
use App\Factory\ProductInRecipeFactory;
use App\Factory\ProductInStockFactory;
use App\Factory\ProductSubCategoryFactory;
use App\Factory\RecipeFactory;
use App\Factory\ShippingAddressFactory;
use App\Factory\StockFactory;
use App\Factory\UserFactory;
use App\Factory\VendorFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // The admin
        UserFactory::createOne([
            'email' => 'eyeinthesky@sirius.com',
            'roles' => ['ROLE_ADMIN', 'ROLE_USER'],
            'password' => '$2y$10$QLjBifXk9NbwkCKzuznQG..PADBxnGQ5AAWDevMI.J/yoc4C9ua8C',
            'lastname' => 'Dumas',
            'firstname' => 'Michel',
            'createdAt' => new \DateTime()
        ]);

        // User with no company
        UserFactory::createOne([
            'email' => 'john@doe.com',
            'roles' => ['ROLE_USER'],
            'password' => '$2y$10$QLjBifXk9NbwkCKzuznQG..PADBxnGQ5AAWDevMI.J/yoc4C9ua8C',
            'lastname' => 'Doe',
            'firstname' => 'John',
            'createdAt' => new \DateTime()
        ]);

        // All the subcategories and sub categories

        $categoriesAndSubcategoriesArray = [
            'La boulangerie' => [
                'Les indispensables',
                'Les extras'
            ]
        ];

        foreach ($categoriesAndSubcategoriesArray as $category => $subCategories) {
            $currentSubcategories = [];

            foreach ($subCategories as $subCategory) {
                $currentSubcategories[] = ProductSubCategoryFactory::createOne(
                    [
                        'name' => $subCategory
                    ]
                );
            }


            ProductCategoryFactory::createOne([
                'name' => $category,
                'SubCategories' => $currentSubcategories,
            ]);
        }

        // All the vendors

        //SuperBake
        $superbakeVendor = UserFactory::createOne([
            'email' => 'fake.bake@urs.com',
            'roles' => ['ROLE_USER', 'ROLE_VENDOR'],
            'password' => '$2a$12$gk3c0OQ9F0I3m.QeFxwS7um2XCMdb/Xrpe9IVb07HmV/Bbbc/xtGq',
            'lastname' => 'Maria',
            'firstname' => 'Thibault',
            'createdAt' => new \DateTime()
        ]);

        $superBakeVendor = VendorFactory::createOne(
            [
                'userAccount' => [$superbakeVendor],
                'name' => 'Super Bake',
                'address' => '10 rue du Poitou, Lille'
            ]
        );

        // All the companies

        // Bakery example
        $bakeryUser = UserFactory::createOne([
            'email' => 'fake.bakery@urs.com',
            'roles' => ['ROLE_USER', 'ROLE_VENDOR'],
            'password' => '$2a$12$gk3c0OQ9F0I3m.QeFxwS7um2XCMdb/Xrpe9IVb07HmV/Bbbc/xtGq',
            'lastname' => 'Stanislas',
            'firstname' => 'Jean',
            'createdAt' => new \DateTime()
        ]);

        $bakeryLastVendors = CompanyLastVendorsManagerFactory::createOne();

        $bakeryCompany = CompanyFactory::createOne([
            'users' => [$bakeryUser],
            'name' => 'La boulangerie sûre',
            'companySize' => 1,
            'email' => 'boulangeriesure@gmail.com',
            'createdAt' => new \DateTime(),
            'shippingAddress' => ShippingAddressFactory::createOne([
                'city' => 'Troyes',
                'address' => '12 rue de la Dîme',
                'zipcode' => '46780'
            ]),
            'companyLastVendors' => $bakeryLastVendors
        ]);

        $productArgsList = [
            [
                'productName' => 'Farine ++',
                'description' => 'Farine de qualité supérieure',
                'measuringUnit' => 'kg',
                'price' => 1000,
                'quantityAvailable' => 5000000,
                'vendor' => $superBakeVendor,
                'productSubCategory' => ProductSubCategoryFactory::random()
            ],
            [
                'productName' => 'Levure Superbake',
                'description' => 'Notre célèbre levure',
                'measuringUnit' => 'kg',
                'price' => 1500,
                'quantityAvailable' => 2300000,
                'vendor' => $superBakeVendor,
                'productSubCategory' => ProductSubCategoryFactory::random()
            ],
            [
                'productName' => 'Sel fin',
                'description' => 'Sel fin provenant de Bretagne',
                'measuringUnit' => 'kg',
                'price' => 10,
                'quantityAvailable' => 3000000,
                'vendor' => $superBakeVendor,
                'productSubCategory' => ProductSubCategoryFactory::random()
            ]
        ];


        foreach ($productArgsList as $productArgs) {
            ProductFactory::createOne($productArgs);
        }

        $recipesList = [
            'Pain' => [
                'recipeObjectFactory' => RecipeFactory::createOne(['company' => $bakeryCompany, 'recipeName' => 'Pain', 'price' => 500]),
                'ingredients' => [
                    [
                        'ingredientObject' => ProductFactory::findBy(['productName' => 'Farine ++'])[0],
                        'quantity' => 0500,
                    ],
                    [
                        'ingredientObject' => ProductFactory::findBy(['productName' => 'Sel fin'])[0],
                        'quantity' => 0005,
                    ],
                    [
                        'ingredientObject' => ProductFactory::findBy(['productName' => 'Levure Superbake'])[0],
                        'quantity' => 0010,
                    ],
                ],
            ]
        ];

        foreach ($recipesList as $recipe) {
            $currentRecipe = $recipe['recipeObjectFactory'];

            foreach ($recipe['ingredients'] as $ingredientArgs) {
                ProductInRecipeFactory::createOne([
                    'Ingredient' => $ingredientArgs['ingredientObject'],
                    'quantity' => $ingredientArgs['quantity'],
                    'recipe' => $currentRecipe
                ]);
            }
        }

        $bakeryStock = StockFactory::createOne(
            ['company' => $bakeryCompany]
        );

        $bakeryStockProducts = [
            ProductFactory::findBy(['productName' => 'Levure Superbake'])[0],
            ProductFactory::findBy(['productName' => 'Sel fin'])[0],
            ProductFactory::findBy(['productName' => 'Farine ++'])[0]
        ];

        foreach ($bakeryStockProducts as $bakeryStockProduct) {
            ProductInStockFactory::createOne([
                'product' => $bakeryStockProduct,
                'stock' => $bakeryStock
            ]);
        }

        $orderList = [
             [
                'orderObjectFactory' => OrderFactory::createOne([
                    'orderCompany' => $bakeryCompany,
                    'status' => 'pending',
                    'price' => 500,
                    'date' => new \DateTime()
                ]),
                'productInOrder' => [
                    [
                        'productObject' => ProductFactory::findBy(['productName' => 'Farine ++'])[0],
                        'quantity' => 0500,
                    ],
                    [
                        'productObject' => ProductFactory::findBy(['productName' => 'Sel fin'])[0],
                        'quantity' => 0005,
                    ],
                    [
                        'productObject' => ProductFactory::findBy(['productName' => 'Levure Superbake'])[0],
                        'quantity' => 0010,
                    ],


                ],
            ]
        ];

        foreach ($orderList as $order) {
            $currentOrder = $order['orderObjectFactory'];

            foreach ($order['productInOrder'] as $productInOrderArgs) {
                $bakeryLastVendors->addVendor($superBakeVendor->object());

                ProductInOrderFactory::createOne([
                    'product' => $productInOrderArgs['productObject'],
                    'quantity' => $productInOrderArgs['quantity'],
                    'OrderEntity' => $currentOrder,
                ]);
            }
        }

        $manager->flush();
    }
}
