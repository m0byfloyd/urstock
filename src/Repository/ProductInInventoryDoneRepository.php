<?php

namespace App\Repository;

use App\Entity\ProductInInventoryDone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProductInInventoryDone>
 *
 * @method ProductInInventoryDone|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductInInventoryDone|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductInInventoryDone[]    findAll()
 * @method ProductInInventoryDone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductInInventoryDoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductInInventoryDone::class);
    }

    public function add(ProductInInventoryDone $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ProductInInventoryDone $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ProductInInventoryDone[] Returns an array of ProductInInventoryDone objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ProductInInventoryDone
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
