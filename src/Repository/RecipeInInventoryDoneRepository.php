<?php

namespace App\Repository;

use App\Entity\RecipeInInventoryDone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RecipeInInventoryDone>
 *
 * @method RecipeInInventoryDone|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecipeInInventoryDone|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecipeInInventoryDone[]    findAll()
 * @method RecipeInInventoryDone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeInInventoryDoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecipeInInventoryDone::class);
    }

    public function add(RecipeInInventoryDone $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(RecipeInInventoryDone $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return RecipeInInventoryDone[] Returns an array of RecipeInInventoryDone objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RecipeInInventoryDone
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
