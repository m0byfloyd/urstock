<?php

namespace App\Repository;

use App\Entity\ProductInStock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductInStock|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductInStock|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductInStock[]    findAll()
 * @method ProductInStock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductInStockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductInStock::class);
    }

    // /**
    //  * @return ProductInStock[] Returns an array of ProductInStock objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductInStock
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getSpecificProductInStock($stockId, $productId)
    {
        //See if possible to reduce the lazyloading of the request

        return $this->createQueryBuilder('productInStock')
            ->select('productInStock')
            ->join('productInStock.Stock', 'Stock')
            ->join('productInStock.Product', 'Product')
            ->where('Stock.id = :stockId')
            ->setParameter('stockId', $stockId)
            ->andWhere('Product.id = :productId')
            ->setParameter('productId', $productId)
            ->getQuery()
            ->getSingleResult();
    }
}
