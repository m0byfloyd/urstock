<?php

namespace App\Repository;

use App\Entity\InventoryDone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<InventoryDone>
 *
 * @method InventoryDone|null find($id, $lockMode = null, $lockVersion = null)
 * @method InventoryDone|null findOneBy(array $criteria, array $orderBy = null)
 * @method InventoryDone[]    findAll()
 * @method InventoryDone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InventoryDoneRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry                $registry,
        private EntityManagerInterface $entityManager
    ) {
        parent::__construct($registry, InventoryDone::class);
    }

    public function add(InventoryDone $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(InventoryDone $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getLast()
    {
        return $this->createQueryBuilder('inventoryDone')
            ->select('inventoryDone.id', 'inventoryDone.date')
            ->orderBy('inventoryDone.date', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    public function getRevenuesSinceAYear($affiliatedStockId): array
    {
        $from = (new \DateTime())->modify('-1 year');
        $months = [];

        for ($i = 0; $i <= 11; $i++) {
            $monthNumber = intval($from->modify('+ 1 month')->format('m'));

            $inventoryForMonth = $this->getRevenuesForAMonth($monthNumber, $affiliatedStockId);

            if (empty($inventoryForMonth)) {
                $months[11 - $i] = [
                    "month" => $from->format('M Y'),
                    "revenues" => 0
                ];
            } else {
                $total = 0;
                $counter = 0;
                foreach ($inventoryForMonth as $inventory) {
                    $counter++;
                    $total += $inventory['revenues'];
                }
                $months[11 - $i] = [
                    "month" => $from->format('M Y'),
                    "revenues" => $total / $counter
                ];
            }
        }

        return $months;
    }

    public function getRevenuesForAMonth($monthNumber, $affiliatedStockId)
    {
        $sql = "SELECT id, revenues FROM inventory_done WHERE date_part('month', date) = ? AND affiliated_stock_id = ?";
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('revenues', 'revenues');
        $query = $this->entityManager->createNativeQuery($sql, $rsm);
        $query->setParameter(1, $monthNumber);
        $query->setParameter(2, $affiliatedStockId);

        return $query->getResult();
    }

    public function getRevenuesForLast31Days($affiliatedStockId): array
    {
        $from = (new \DateTime())->modify('-1 month');
        $days = [];
        $dayNumber = intval($from->format('d'));

        for ($i = 0; $i <= 30; $i++) {
            $monthNumber = intval($from->format('m'));
            $inventoryForDay = $this->getRevenuesForADay($dayNumber, $monthNumber, $affiliatedStockId);

            if (empty($inventoryForDay)) {
                $days[30 - $i] = [
                    "day" => $from->format('d M'),
                    "revenues" => 0
                ];
            } else {
                $total = 0;
                $counter = 0;
                foreach ($inventoryForDay as $inventory) {
                    $counter++;
                    $total += $inventory['revenues'];
                }
                $days[30 - $i] = [
                    "day" => $from->format('d M'),
                    "revenues" => $total / $counter
                ];
            }

            $dayNumber = intval($from->modify('+ 1 day')->format('d'));
        }

        return $days;
    }

    //TODO: Find a better way instead of $monthNumber
    public function getRevenuesForADay($dayNumber, $monthNumber, $affiliatedStockId)
    {
        $sql = "SELECT id, revenues FROM inventory_done WHERE date_part('day', date) = ? AND date_part('month', date) = ? AND affiliated_stock_id = ?";
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('revenues', 'revenues');
        $query = $this->entityManager->createNativeQuery($sql, $rsm);
        $query->setParameter(1, $dayNumber);
        $query->setParameter(2, $monthNumber);
        $query->setParameter(3, $affiliatedStockId);

        return $query->getResult();
    }

    public function getRevenuesSinceAWeek($affiliatedStockId)
    {
        $from = (new \DateTime())->modify('-1 week');
        $days = [];
        $dayNumber = intval($from->format('d'));

        for ($i = 0; $i <= 7; $i++) {
            $monthNumber = intval($from->format('m'));
            $inventoryForDay = $this->getRevenuesForADay($dayNumber, $monthNumber, $affiliatedStockId);

            if (empty($inventoryForDay)) {
                $days[7 - $i] = [
                    "day" => $from->format('d M'),
                    "revenues" => 0
                ];
            } else {
                $total = 0;
                $counter = 0;
                foreach ($inventoryForDay as $inventory) {
                    $counter++;
                    $total += $inventory['revenues'];
                }
                $days[7 - $i] = [
                    "day" => $from->format('d M'),
                    "revenues" => $total / $counter
                ];
            }

            $dayNumber = intval($from->modify('+ 1 day')->format('d'));
        }

        return $days;
    }

//    /**
//     * @return InventoryDone[] Returns an array of InventoryDone objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?InventoryDone
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
