<?php

namespace App\Repository;

use App\Entity\CompanyLastVendorsManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CompanyLastVendorsManager>
 *
 * @method CompanyLastVendorsManager|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyLastVendorsManager|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyLastVendorsManager[]    findAll()
 * @method CompanyLastVendorsManager[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyLastVendorsManagerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyLastVendorsManager::class);
    }

    public function add(CompanyLastVendorsManager $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CompanyLastVendorsManager $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return CompanyLastVendorsManager[] Returns an array of CompanyLastVendorsManager objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CompanyLastVendorsManager
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
