<?php

namespace App\Controller\Admin;

use App\Entity\ProductSubCategory;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProductSubCategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductSubCategory::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
