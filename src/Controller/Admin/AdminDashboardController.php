<?php

namespace App\Controller\Admin;

use App\Entity\Company;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\ProductCategory;
use App\Entity\ProductInOrder;
use App\Entity\ProductInRecipe;
use App\Entity\ProductInStock;
use App\Entity\ProductSubCategory;
use App\Entity\Recipe;
use App\Entity\Stock;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('App');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('User', 'fas fa-list', User::class);
        yield MenuItem::linkToCrud('Stock', 'fas fa-list', Stock::class);
        yield MenuItem::linkToCrud('Recipe', 'fas fa-list', Recipe::class);
        yield MenuItem::linkToCrud('Company', 'fas fa-list', Company::class);
        yield MenuItem::linkToCrud('Order', 'fas fa-list', Order::class);
        yield MenuItem::linkToCrud('ProductInOrder', 'fas fa-list', ProductInOrder::class);
        yield MenuItem::linkToCrud('ProductCategory', 'fas fa-list', ProductCategory::class);
        yield MenuItem::linkToCrud('ProductSubCategory', 'fas fa-list', ProductSubCategory::class);
        yield MenuItem::linkToCrud('Product', 'fas fa-list', Product::class);
        yield MenuItem::linkToCrud('ProductInRecipe', 'fas fa-list', ProductInRecipe::class);
        yield MenuItem::linkToCrud('ProductInStock', 'fas fa-list', ProductInStock::class);
    }
}
