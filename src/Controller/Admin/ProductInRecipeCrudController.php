<?php

namespace App\Controller\Admin;

use App\Entity\ProductInRecipe;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProductInRecipeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductInRecipe::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
