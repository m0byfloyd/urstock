<?php

namespace App\Controller\Admin;

use App\Entity\ProductInStock;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProductInStockCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductInStock::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
