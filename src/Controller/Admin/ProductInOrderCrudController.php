<?php

namespace App\Controller\Admin;

use App\Entity\ProductInOrder;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProductInOrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductInOrder::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
