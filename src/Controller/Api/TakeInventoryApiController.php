<?php

namespace App\Controller\Api;

use App\Controller\Api\Helpers\ParametersHelper;
use App\Controller\Api\Helpers\RequestHelper;
use App\Controller\Helpers\StockHelper;
use App\Controller\Helpers\UserHelper;
use App\Entity\InventoryDone;
use App\Entity\ProductInInventoryDone;
use App\Entity\RecipeInInventoryDone;
use App\Repository\InventoryDoneRepository;
use App\Repository\ProductInInventoryDoneRepository;
use App\Repository\ProductRepository;
use App\Repository\RecipeRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TakeInventoryApiController extends AbstractController
{
    /**
     * @throws Exception
     * @throws NonUniqueResultException
     */
    public function newInventoryTaken(
        EntityManagerInterface $entityManager,
        Request                $request,
        UserRepository         $userRepository,
        ProductRepository $productRepository,
        RecipeRepository $recipeRepository,
        InventoryDoneRepository $inventoryDoneRepository,
        ProductInInventoryDoneRepository $productInInventoryDoneRepository
    ): \Symfony\Component\HttpFoundation\JsonResponse {
        // TODO: Vérifier si les recettes existent

        $lastInventoryToCheck = $inventoryDoneRepository->getLast();
        if ($lastInventoryToCheck) {
            $today = date("Y-m-d");
            $expire = $lastInventoryToCheck[0]['date']->format("Y-m-d");

            if ($expire === $today) {
                throw new HttpException(409, 'The inventory of today has been already taken');
            }
        }

        $parameters = RequestHelper::getParameters($request, ParametersHelper::TAKE_INVENTORY);

        $currentUser = UserHelper::getCurrentUser($userRepository, $this->getUser()->getUserIdentifier());
        $currentStock = StockHelper::getCurrentStock($currentUser, $userRepository);

        $productsInInventoryDone = [];
        $recipesInInventoryDone = [];
        $revenues = 0;

        foreach ($parameters['products'] as $product) {
            $productsInInventoryDone[] = (new ProductInInventoryDone())
                ->setQuantity($product['quantity'])
                ->setProduct($productRepository->find($product['id']));
        }

        foreach ($parameters['recipesSold'] as $recipesSold) {
            $recipe = $recipeRepository->find($recipesSold['id']);
            $quantity = $recipesSold['quantity'];
            $revenues += $recipe->getPrice() * $quantity;
            $recipesInInventoryDone[] = (new RecipeInInventoryDone())
                ->setQuantity($quantity)
                ->setRecipe($recipe);
        }

        $note = $parameters['note'] ?: '';

        try {
            $inventoryDone = (new InventoryDone())
                ->setAuthor($currentUser)
                ->setNote($note)
                ->setAffiliatedStock($currentStock)
                ->setDate(new \DateTime())
                ->setRevenues($revenues);

            foreach ($recipesInInventoryDone as $recipeInInventoryDone) {
                $inventoryDone->addRecipeInInventoryDone($recipeInInventoryDone);
            }

            foreach ($productsInInventoryDone as $productInInventoryDone) {
                $inventoryDone->addProductInInventoryDone($productInInventoryDone);
            }

            $entityManager->persist($inventoryDone);
            $entityManager->flush();

            return $this->json('Inventory Taken');
        } catch (Exception $error) {
            throw new Exception($error);
        }
    }
}
