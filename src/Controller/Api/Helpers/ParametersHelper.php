<?php

namespace App\Controller\Api\Helpers;

class ParametersHelper
{
    public const UPDATE_QUANTITY_OF_PRODUCT_IN_STOCK = self::ADD_PRODUCT_IN_STOCK;


    /* --- Company --- */
    public const COMPANY_CREATE = [
        'phoneNumber',
        'siret',
        'name',
        'companySize',
        'email'
    ];

    public const COMPANY_UPDATE_INFORMATIONS = [
        'email',
        'name',
        'phoneNumber',
        'companySize'
    ];


    /* --- Inventory --- */

    public const TAKE_INVENTORY = [
        'products',
        'recipesSold'
    ];


    /* --- Order --- */

    public const ORDER_CREATE = [
        'price',
        'items'
    ];

    /* --- Recipe --- */

    public const RECIPE_CREATE = [
        'name',
        'price',
        'items'
    ];

    /* --- Shipping address --- */

    public const SHIPPING_ADDRESS_SET = [
        'city',
        'address',
        'zipCode',
        'complementOfAddress'
    ];


    public const SHIPPING_ADDRESS_UPDATE = self::SHIPPING_ADDRESS_SET;


    /* --- Statistics --- */

    public const STATISTICS_REVENUES_GET = [
        'time'
    ];


    /* --- Stock --- */

    public const STOCK_DELETE_PRODUCT = [
        'productId',
    ];

    public const ADD_PRODUCT_IN_STOCK = [
        'productId',
        'quantity'
    ];


    /* --- User --- */

    public const REGISTRATION = [
        'email',
        'firstname',
        'lastname',
        'password',
        'phoneNumber'
    ];

    public const USER_UPDATE = [
        'email',
        'firstname',
        'lastname',
        'password',
    ];
}
