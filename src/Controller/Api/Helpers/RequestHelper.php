<?php

namespace App\Controller\Api\Helpers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class RequestHelper
{
    public function __construct(
        Request $request
    ) {
    }

    public static function getParameters(
        Request $request,
        $requiredParameters,
        $returnArray = true
    ) {
        self::checkIfRequestIsGood($request);

        $parameters = json_decode($request->getContent(), true);

        self::checkParameters($requiredParameters, $parameters);

        if (!$returnArray) {
            $parameters = json_decode($request->getContent(), false);
        }

        return $parameters;
    }

    public static function checkIfRequestIsGood(
        $request
    ): void {
        if (empty($request->getContent())) {
            throw new HttpException(500, 'Something went wrong');
        }
    }

    public static function checkParameters(
        $requiredParameters,
        $parametersToCheck
    ): void {
        for ($i = 0; $i < count($requiredParameters); $i++) {
            foreach ($requiredParameters as $requiredParameter) {
                if (!in_array($requiredParameter, array_keys($parametersToCheck))) {
                    throw new HttpException(422, 'Missing following parameter : ' . $requiredParameter);
                }
            }
        }
    }
}
