<?php

namespace App\Controller\Api;

use App\Controller\Api\Helpers\ParametersHelper;
use App\Controller\Api\Helpers\RequestHelper;
use App\Controller\Helpers\CompanyHelper;
use App\Entity\ProductInRecipe;
use App\Entity\Recipe;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class RecipeApiController extends AbstractController
{
    /**
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function createRecipe(
        UserInterface $user,
        UserRepository $userRepository,
        ProductRepository $productRepository,
        Request $request,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $currentCompany = CompanyHelper::getCurrentCompany($this->getUser(), $userRepository);
        $parameters = RequestHelper::getParameters($request, ParametersHelper::RECIPE_CREATE);

        try {
            $recipe = new Recipe();
            $recipe->setRecipeName($parameters['name'])
                ->setPrice($parameters['price'])
                ->setCompany($currentCompany);

            foreach ($parameters['items'] as $product) {
                $ingredients = $productRepository->find($product['id']);

                $productInRecipe = new ProductInRecipe();
                $productInRecipe
                    ->setIngredient($ingredients)
                    ->setQuantity($product['quantity']);
                $entityManager->persist($productInRecipe);
                $recipe->addProductInRecipe($productInRecipe);
            }

            $entityManager->persist($recipe);
            $entityManager->flush();
        } catch (Exception $exception) {
            throw new Exception();
        }

        return $this->json($recipe);
    }
}
