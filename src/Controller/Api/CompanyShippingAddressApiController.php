<?php

namespace App\Controller\Api;

use App\Controller\Api\Helpers\ParametersHelper;
use App\Controller\Api\Helpers\RequestHelper;
use App\Controller\Helpers\CompanyHelper;
use App\Controller\Helpers\ShippingAddressHelper;
use App\Entity\ShippingAddress;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CompanyShippingAddressApiController extends AbstractController
{
    /**
     * @throws NonUniqueResultException
     */
    public function getShippingAddress(UserRepository $userRepository): ?ShippingAddress
    {
        return ShippingAddressHelper::getCurrentShippingAddress($this->getUser(), $userRepository);
    }

    /**
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function setShippingAddressToCompany(
        UserRepository         $userRepository,
        Request                $request,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $currentCompany = CompanyHelper::getCurrentCompany($this->getUser(), $userRepository);

        if ($currentCompany->getShippingAddress()) {
            throw new HttpException(409, 'Company already has an address');
        }

        $parameters = RequestHelper::getParameters($request, ParametersHelper::SHIPPING_ADDRESS_SET);
        try {
            $shippingAddress =  (new ShippingAddress())
                ->setAddress($parameters['address'])
                ->setCity($parameters['city'])
                ->setComplementOfAddress($parameters['complementOfAddress'])
                ->setZipcode($parameters['zipCode']);

            $currentCompany->setShippingAddress($shippingAddress);
            $entityManager->persist($currentCompany);
            $entityManager->flush();
        } catch (Exception $exception) {
            throw new Exception($exception);
        }

        return $this->json($shippingAddress);
    }


    /**
     * @throws Exception
     */
    public function updateShippingAddress(
        Request                $request,
        UserRepository         $userRepository,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $parameters = RequestHelper::getParameters($request, ParametersHelper::SHIPPING_ADDRESS_UPDATE);
        $currentShippingAddress = ShippingAddressHelper::getCurrentShippingAddress($this->getUser(), $userRepository);

        try {
            $currentShippingAddress
                ->setAddress($parameters['address'])
                ->setCity($parameters['city'])
                ->setComplementOfAddress($parameters['complementOfAddress'])
                ->setZipcode($parameters['zipCode']);

            $entityManager->persist($currentShippingAddress);
            $entityManager->flush();
        } catch (Exception $exception) {
            throw new Exception($exception);
        }

        return $this->json($currentShippingAddress);
    }
}
