<?php

namespace App\Controller\Api;

use App\Controller\Api\Helpers\ParametersHelper;
use App\Controller\Api\Helpers\RequestHelper;
use App\Controller\Helpers\StockHelper;
use App\Repository\InventoryDoneRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class StatisticsApiController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository
    ) {
    }

    //TODO : Found a better way
    public function getRevenuesStatistics(
        Request $request,
        InventoryDoneRepository $inventoryDoneRepository
    ): \Symfony\Component\HttpFoundation\JsonResponse {
        $parameters = RequestHelper::getParameters($request, ParametersHelper::STATISTICS_REVENUES_GET);
        $currentStockId = StockHelper::getCurrentStock($this->getUser(), $this->userRepository)->getId();

        $datas = match ($parameters['time']) {
            'week' => $inventoryDoneRepository->getRevenuesSinceAWeek($currentStockId),
            'month' => $inventoryDoneRepository->getRevenuesForLast31Days($currentStockId),
            'year' => $inventoryDoneRepository->getRevenuesSinceAYear($currentStockId),
            default => null,
        };

        return $this->json($datas);
    }
}
