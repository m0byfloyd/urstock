<?php

namespace App\Controller\Api;

use App\Controller\Api\Helpers\ParametersHelper;
use App\Controller\Api\Helpers\RequestHelper;
use App\Controller\Helpers\StockHelper;
use App\Entity\ProductInStock;
use App\Entity\Stock;
use App\Repository\ProductInStockRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class StockApiController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
    }

    public function getStockInformations(): \Symfony\Component\HttpFoundation\JsonResponse
    {
        return $this->json(StockHelper::getCurrentStock($this->getUser(), $this->userRepository));
    }

    /**
     * @throws Exception
     */
    public function addProductInStock(
        EntityManagerInterface $entityManager,
        ProductRepository      $productRepository,
        Request                $request,
    ): \Symfony\Component\HttpFoundation\JsonResponse {
        $parameters = RequestHelper::getParameters($request, ParametersHelper::ADD_PRODUCT_IN_STOCK);
        $stock = StockHelper::getCurrentStock($this->getUser(), $this->userRepository);

        $product = $productRepository->find($parameters['productId']);

        if (!$product) {
            throw new HttpException(404, 'The product didn\'t exist');
        }

        try {
            $entityManager->persist(
                (new ProductInStock())
                    ->setStock($stock)
                    ->setProduct($product)
                    ->setQuantity($parameters['quantity'])
            );
            $entityManager->flush();
        } catch (Exception $error) {
            throw new Exception($error);
        }

        return $this->json($stock);
    }


    /**
     * @throws Exception
     */
    public function updateQuantityOfProductInStock(
        Request                  $request,
        EntityManagerInterface   $entityManager,
        ProductRepository        $productRepository,
        ProductInStockRepository $productInStockRepository
    ): \Symfony\Component\HttpFoundation\JsonResponse {
        $stock = StockHelper::getCurrentStock($this->getUser(), $this->userRepository);
        $parameters = RequestHelper::getParameters($request, ParametersHelper::UPDATE_QUANTITY_OF_PRODUCT_IN_STOCK);
        $product = $productRepository->find($parameters['productId']);

        if (!$product) {
            throw new HttpException(404, 'The product didn\'t exist');
        }

        try {
            $productInStock = $productInStockRepository->getSpecificProductInStock($stock->getId(), $parameters['productId']);
            $productInStock->setQuantity($parameters['quantity']);
            $entityManager->flush();
        } catch (Exception|NoResultException|NonUniqueResultException $e) {
            throw new Exception($e);
        }

        return $this->json($productInStock);
    }

    /**
     * @throws Exception
     */
    public function deleteProductInStock(
        Request                  $request,
        EntityManagerInterface   $entityManager,
        ProductInStockRepository $productInStockRepository
    ): \Symfony\Component\HttpFoundation\JsonResponse {
        $parameters = RequestHelper::getParameters($request, ParametersHelper::STOCK_DELETE_PRODUCT, );
        $stock = StockHelper::getCurrentStock($this->getUser(), $this->userRepository);

        try {
            $entityManager->remove(
                $productInStockRepository->getSpecificProductInStock(
                    $stock->getId(),
                    $parameters['productId']
                )
            );
            $entityManager->flush();
        } catch (Exception|NoResultException|NonUniqueResultException $e) {
            throw new Exception($e);
        }

        return $this->json($stock);
    }
}
