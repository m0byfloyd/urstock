<?php

namespace App\Controller\Api;

use App\Controller\Api\Helpers\ParametersHelper;
use App\Controller\Api\Helpers\RequestHelper;
use App\Controller\Helpers\CompanyHelper;
use App\Entity\Company;
use App\Entity\CompanyLastVendorsManager;
use App\Entity\ProductInRecipe;
use App\Entity\Recipe;
use App\Entity\Stock;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CompanyApiController extends AbstractController
{

    /**
     * @throws NonUniqueResultException
     */
    public function getInformations(UserRepository $userRepository): ?\App\Entity\Company
    {
        return CompanyHelper::getCurrentCompany($this->getUser(), $userRepository);
    }

    /**
     * @throws Exception
     */
    public function updateInformations(
        UserRepository $userRepository,
        Request $request,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $parameters = RequestHelper::getParameters($request, ParametersHelper::COMPANY_UPDATE_INFORMATIONS);
        $currentCompany = CompanyHelper::getCurrentCompany($this->getUser(), $userRepository);

        try {
            $currentCompany
                ->setEmail($parameters['email'])
                ->setName($parameters['name'])
                ->setPhoneNumber($parameters['phoneNumber'])
                ->setCompanySize($parameters['companySize']);

            $entityManager->persist($currentCompany);
            $entityManager->flush();
        } catch (Exception $exception) {
            throw new Exception($exception);
        }

        return $this->json($currentCompany);
    }

    // TODO: Make new function with pagination

    /**
     * @throws NonUniqueResultException
     */
    public function getOrders(UserRepository $userRepository): JsonResponse
    {
        $orders = CompanyHelper::getCurrentCompany($this->getUser(), $userRepository)->getOrders();
        if (count($orders) === 0) {
            throw new HttpException(404, 'Company don\'t have any order');
        }
        return $this->json($orders);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getRecipes(UserRepository $userRepository): JsonResponse
    {
        $recipes = CompanyHelper::getCurrentCompany($this->getUser(), $userRepository)->getRecipes();
        if (count($recipes) === 0) {
            throw new HttpException(404, 'Company don\'t have any recipe');
        }
        return $this->json($recipes);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getLastVendors(UserRepository $userRepository): HttpException|JsonResponse
    {
        $lastVendors = CompanyHelper::getCurrentCompany($this->getUser(), $userRepository)->getCompanyLastVendors()->getVendors();

        if (count($lastVendors) === 0) {
            throw new HttpException(404, ('Company doesn\'t ordered to any vendor yet'));
        }
        return $this->json($lastVendors);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function createCompanyForUser(
        Request                $request,
        UserRepository         $userRepository,
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository
    ): JsonResponse {
        $currentUser = $this->getUser();

        if ($this->getUser()->hasCompany()) {
            throw new HttpException(409, 'User already has a company');
        }

        $parameters = RequestHelper::getParameters($request, ParametersHelper::COMPANY_CREATE);

        $companyUser = $userRepository->findOneByEmail($currentUser->getUserIdentifier());

        $company = (new Company())
            ->setCreatedAt(new \DateTime())
            ->setPhoneNumber($parameters['phoneNumber'])
            ->setSiret($parameters['siret'])
            ->setStock(new Stock())
            ->setName($parameters['name'])
            ->setCompanySize($parameters['companySize'])
            ->setEmail($parameters['email'])
            ->addUser($companyUser)
            ->setCompanyLastVendors(new CompanyLastVendorsManager());


        $sampleRecipe = (new Recipe())
            ->setRecipeName('Recette d\'exemple')
            ->setCompany($company)
            ->setPrice(10);

        $productInRecipe = (new ProductInRecipe())
            ->setIngredient($productRepository->find(1))
            ->setRecipe($sampleRecipe)
            ->setQuantity(2);

        $entityManager->persist($company);
        $entityManager->persist($sampleRecipe);
        $entityManager->persist($productInRecipe);
        $entityManager->flush();

        return $this->json($company);
    }
}
