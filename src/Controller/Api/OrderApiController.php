<?php

namespace App\Controller\Api;

use App\Controller\Api\Helpers\ParametersHelper;
use App\Controller\Api\Helpers\RequestHelper;
use App\Controller\Helpers\CompanyHelper;
use App\Entity\Order;
use App\Entity\ProductInOrder;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class OrderApiController extends AbstractController
{
    /**
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function createOrder(
        UserInterface $user,
        UserRepository $userRepository,
        ProductRepository $productRepository,
        Request $request,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $currentCompany = CompanyHelper::getCurrentCompany($this->getUser(), $userRepository);
        $parameters = RequestHelper::getParameters($request, ParametersHelper::ORDER_CREATE);

        try {
            $order = new Order();
            $order->setPrice($parameters['price'])
                ->setDate(new \DateTime())
                ->setOrderCompany($currentCompany)
                ->setStatus('pending');

            foreach ($parameters['items'] as $product) {
                $productInCart = $productRepository->find($product['id']);

                $productInOrder = new ProductInOrder();
                $productInOrder
                    ->setProduct($productInCart)
                    ->setQuantity($product['quantity']);

                $entityManager->persist($productInOrder);
                $order->addProductInOrder($productInOrder);
            }

            $entityManager->persist($order);
            $entityManager->flush();
        } catch (Exception $exception) {
            throw new Exception();
        }

        return $this->json($order);
    }
}
