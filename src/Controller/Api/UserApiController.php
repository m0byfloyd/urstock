<?php

namespace App\Controller\Api;

use App\Controller\Api\Helpers\ParametersHelper;
use App\Controller\Api\Helpers\RequestHelper;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserApiController extends AbstractController
{
    public function getMe(): \Symfony\Component\Security\Core\User\UserInterface
    {
        return $this->getUser();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function updateMe(
        Request                $request,
        UserRepository         $userRepository,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $currentUser = $userRepository->findOneByEmail($this->getUser()->getUserIdentifier());

        $arrayToCheck = [
            'lastname',
            'firstname',
            'email',
            'password'
        ];

        if (!$currentUser) {
            throw new HttpException(404, 'Current user not found');
        }

        $parameters = RequestHelper::getParameters($request, ParametersHelper::USER_UPDATE);

        $currentUser->setEmail($parameters['email'])
            ->setFirstname($parameters['firstname'])
            ->setLastname($parameters['lastname'])
            ->setPassword($parameters['password']);

        $entityManager->persist($currentUser);
        $entityManager->flush();

        return new JsonResponse('Informations changed');
    }
}
