<?php

namespace App\Controller;

use App\Controller\Api\Helpers\ParametersHelper;
use App\Controller\Api\Helpers\RequestHelper;
use App\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;
use ErrorException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository
    ) {
    }

    /**
     * @throws ErrorException
     * @throws NonUniqueResultException
     */
    #[Route(path: '/register', name: 'app_register', methods: ['POST'])]
    public function register(Request $request, JWTTokenManagerInterface $JWTTokenManager): JsonResponse
    {
        $parameters = RequestHelper::getParameters($request, ParametersHelper::REGISTRATION, false);

        $user = $this->userRepository->createNewUser($parameters);

        return new JsonResponse(['token' => $JWTTokenManager->create($user) ]);
    }

    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('admin');
        }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): \Symfony\Component\HttpFoundation\Response
    {
    }
}
