<?php

namespace App\Controller\Helpers;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class StockHelper extends AbstractController
{
    public static function getCurrentStock($user, $userRepository): \App\Entity\Stock
    {
        try {
            return CompanyHelper::getCurrentCompany($user, $userRepository)->getStock();
        } catch (\Exception) {
            throw new HttpException(404, 'The company don\'t get a stock');
        }
    }
}
