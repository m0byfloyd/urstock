<?php

namespace App\Controller\Helpers;

use App\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CompanyHelper extends AbstractController
{
    /**
     * @throws NonUniqueResultException
     */
    public static function getCurrentCompany(UserInterface $user, UserRepository $userRepository): ?\App\Entity\Company
    {
        $currentUser = $userRepository->findOneByEmail($user->getUserIdentifier());

        if (!$currentUser->userHasCompany()) {
            throw new HttpException(404, 'User don\'t have a company');
        }

        return $currentUser->getCompany();
    }
}
