<?php

namespace App\Controller\Helpers;

use App\Entity\ShippingAddress;
use App\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ShippingAddressHelper extends AbstractController
{
    /**
     * @throws NonUniqueResultException
     */
    public static function getCurrentShippingAddress($user, UserRepository $userRepository): ?ShippingAddress
    {
        return CompanyHelper::getCurrentCompany($user, $userRepository)->getShippingAddress()
            ? CompanyHelper::getCurrentCompany($user, $userRepository)->getShippingAddress()
            : throw new HttpException(404, 'Company don\'t have a shipping address');
    }
}
