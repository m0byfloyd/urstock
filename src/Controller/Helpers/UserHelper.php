<?php

namespace App\Controller\Helpers;

use App\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserHelper extends AbstractController
{
    /**
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public static function getCurrentUser(
        UserRepository $userRepository,
                       $userEmail
    ) {
        try {
            return $userRepository->findOneByEmail($userEmail);
        } catch (Exception $exception) {
            throw new Exception($exception);
        }
    }
}
