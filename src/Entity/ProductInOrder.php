<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductInOrderRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    normalizationContext: [
        'groups' => [
            'orderDetail'
        ]
    ]
)]
#[ORM\Entity(repositoryClass: ProductInOrderRepository::class)]
class ProductInOrder
{
    #[Groups(["orderDetail"])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'productInOrders')]
    #[ORM\JoinColumn(nullable: false)]
    private $OrderEntity;

    #[Groups("orderDetail")]
    #[ORM\ManyToOne(targetEntity: Product::class, cascade: ['persist', 'remove'], inversedBy: 'productInOrder')]
    #[ORM\JoinColumn(nullable: false)]
    private $Product;

    #[Groups("orderDetail")]
    #[ORM\Column(type: 'integer')]
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderEntity(): ?Order
    {
        return $this->OrderEntity;
    }

    public function setOrderEntity(?Order $OrderEntity): self
    {
        $this->OrderEntity = $OrderEntity;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->Product;
    }

    public function setProduct(Product $Product): self
    {
        $this->Product = $Product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
