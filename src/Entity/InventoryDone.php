<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\InventoryDoneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InventoryDoneRepository::class)]
#[ApiResource(
    collectionOperations: [
        'collName_api_take_inventory' => [
            'route_name' => 'api_take_inventory'
        ]
    ]
)]
class InventoryDone
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\ManyToOne(targetEntity: Stock::class, inversedBy: 'inventoryDones')]
    #[ORM\JoinColumn(nullable: false)]
    private $AffiliatedStock;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'inventoryDones')]
    #[ORM\JoinColumn(nullable: false)]
    private $Author;

    #[ORM\Column(type: 'text', nullable: true)]
    private $note;

    #[ORM\OneToMany(mappedBy: 'InventoryDone', targetEntity: ProductInInventoryDone::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    private $productInInventoryDones;

    #[ORM\Column(type: 'integer')]
    private $revenues;

    #[ORM\OneToMany(mappedBy: 'InventoryDone', targetEntity: RecipeInInventoryDone::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    private $recipeInInventoryDones;

    public function __construct()
    {
        $this->productInInventoryDones = new ArrayCollection();
        $this->recipeInInventoryDones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAffiliatedStock(): ?Stock
    {
        return $this->AffiliatedStock;
    }

    public function setAffiliatedStock(?Stock $AffiliatedStock): self
    {
        $this->AffiliatedStock = $AffiliatedStock;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->Author;
    }

    public function setAuthor(?User $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return Collection<int, ProductInInventoryDone>
     */
    public function getProductInInventoryDones(): Collection
    {
        return $this->productInInventoryDones;
    }

    public function addProductInInventoryDone(ProductInInventoryDone $productInInventoryDone): self
    {
        if (!$this->productInInventoryDones->contains($productInInventoryDone)) {
            $this->productInInventoryDones[] = $productInInventoryDone;
            $productInInventoryDone->setInventoryDone($this);
        }

        return $this;
    }

    public function removeProductInInventoryDone(ProductInInventoryDone $productInInventoryDone): self
    {
        if ($this->productInInventoryDones->removeElement($productInInventoryDone)) {
            // set the owning side to null (unless already changed)
            if ($productInInventoryDone->getInventoryDone() === $this) {
                $productInInventoryDone->setInventoryDone(null);
            }
        }

        return $this;
    }

    public function getRevenues(): ?int
    {
        return $this->revenues;
    }

    public function setRevenues(int $revenues): self
    {
        $this->revenues = $revenues;

        return $this;
    }

    /**
     * @return Collection<int, RecipeInInventoryDone>
     */
    public function getRecipeInInventoryDones(): Collection
    {
        return $this->recipeInInventoryDones;
    }

    public function addRecipeInInventoryDone(RecipeInInventoryDone $recipeInInventoryDone): self
    {
        if (!$this->recipeInInventoryDones->contains($recipeInInventoryDone)) {
            $this->recipeInInventoryDones[] = $recipeInInventoryDone;
            $recipeInInventoryDone->setInventoryDone($this);
        }

        return $this;
    }

    public function removeRecipeInInventoryDone(RecipeInInventoryDone $recipeInInventoryDone): self
    {
        if ($this->recipeInInventoryDones->removeElement($recipeInInventoryDone)) {
            // set the owning side to null (unless already changed)
            if ($recipeInInventoryDone->getInventoryDone() === $this) {
                $recipeInInventoryDone->setInventoryDone(null);
            }
        }

        return $this;
    }
}
