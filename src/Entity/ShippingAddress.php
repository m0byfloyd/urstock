<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ShippingAddressRepository;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    collectionOperations: [
        'get',
        'post',
        'collName_api_current_company_shipping_address' => [
            'route_name' => 'api_current_company_shipping_address',
        ],
        'collName_api_current_company_update_shipping_address' => [
            'route_name' => 'api_current_company_update_shipping_address',
        ]
    ],
)]
#[ORM\Entity(repositoryClass: ShippingAddressRepository::class)]
class ShippingAddress
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 60)]
    private $city;

    #[ORM\Column(type: 'string', length: 100)]
    private $address;

    #[ORM\Column(type: 'string', length: 5)]
    private $zipcode;

    #[ORM\OneToOne(inversedBy: 'shippingAddress', targetEntity: Company::class, cascade: ['persist', 'remove'])]
    private $CompanyAddress;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private $complementOfAddress;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCompanyAddress(): ?Company
    {
        return $this->CompanyAddress;
    }

    public function setCompanyAddress(?Company $CompanyAddress): self
    {
        $this->CompanyAddress = $CompanyAddress;

        return $this;
    }

    public function getComplementOfAddress(): ?string
    {
        return $this->complementOfAddress;
    }

    public function setComplementOfAddress(?string $complementOfAddress): self
    {
        $this->complementOfAddress = $complementOfAddress;

        return $this;
    }
}
