<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    collectionOperations: [
        'get',
        'collName_api_current_company_order' => [
            'route_name' => 'api_current_company_orders',
            'normalization_context' => ['groups' => 'orderDetail'],
        ],
        'collName_api_order_create_new_from_cart' => [
            'route_name' => 'api_current_company_order_create_new_from_cart',
            'method' => 'POST'
        ],

    ],
    itemOperations: [
        'get' => [
            "security" => "is_granted('ORDER_VIEW', object)"
        ]
    ],
    normalizationContext: ['groups' => ['orderDetail']]
)]

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[Groups(['orderDetail'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['orderDetail'])]
    #[ORM\Column(type: 'string', length: 20)]
    private $status;

    #[Groups(['orderDetail'])]
    #[ORM\ManyToOne(targetEntity: Company::class, inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    private $orderCompany;

    #[Groups(['orderDetail'])]
    #[ORM\OneToMany(mappedBy: 'OrderEntity', targetEntity: ProductInOrder::class, orphanRemoval: true)]
    private $productInOrders;

    #[Groups(['orderDetail'])]
    #[ORM\Column(type: 'integer')]
    private $price;

    #[Groups(['orderDetail'])]
    #[ORM\Column(type: 'date')]
    private $date;

    public function __construct()
    {
        $this->productInOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getOrderCompany(): ?Company
    {
        return $this->orderCompany;
    }

    public function setOrderCompany(?Company $orderCompany): self
    {
        $this->orderCompany = $orderCompany;

        return $this;
    }

    /**
     * @return Collection<int, ProductInOrder>
     */
    public function getProductInOrders(): Collection
    {
        return $this->productInOrders;
    }

    public function addProductInOrder(ProductInOrder $productInOrder): self
    {
        if (!$this->productInOrders->contains($productInOrder)) {
            $this->productInOrders[] = $productInOrder;
            $productInOrder->setOrderEntity($this);
        }

        return $this;
    }

    public function removeProductInOrder(ProductInOrder $productInOrder): self
    {
        if ($this->productInOrders->removeElement($productInOrder)) {
            // set the owning side to null (unless already changed)
            if ($productInOrder->getOrderEntity() === $this) {
                $productInOrder->setOrderEntity(null);
            }
        }

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
