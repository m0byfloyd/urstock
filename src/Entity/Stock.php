<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StockRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    collectionOperations: [
        'get',
        'post',
        'collName_api_current_company_stock' => [
            'route_name' => 'api_current_company_stock',
        ]
    ],
)]
#[ORM\Entity(repositoryClass: StockRepository::class)]
class Stock
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToOne(inversedBy: 'stock', targetEntity: Company::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private $company;

    #[ORM\OneToMany(mappedBy: 'Stock', targetEntity: ProductInStock::class, orphanRemoval: true)]
    private $productInStock;

    #[ORM\OneToMany(mappedBy: 'AffiliatedStock', targetEntity: InventoryDone::class, orphanRemoval: true)]
    private $inventoryDones;

    public function __construct()
    {
        $this->productInStock = new ArrayCollection();
        $this->inventoryDones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProductInStock(): Collection
    {
        return $this->productInStock;
    }

    public function addProductInStock(ProductInStock $productInStock): self
    {
        if (!$this->productInStock->contains($productInStock)) {
            $this->productInStock[] = $productInStock;
            $productInStock->setStock($this);
        }

        return $this;
    }

    public function removeProductInStock(ProductInStock $productInStock): self
    {
        if ($this->productInStock->removeElement($productInStock)) {
            // set the owning side to null (unless already changed)
            if ($productInStock->getStock() === $this) {
                $productInStock->setStock(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, InventoryDone>
     */
    public function getInventoryDones(): Collection
    {
        return $this->inventoryDones;
    }

    public function addInventoryDone(InventoryDone $inventoryDone): self
    {
        if (!$this->inventoryDones->contains($inventoryDone)) {
            $this->inventoryDones[] = $inventoryDone;
            $inventoryDone->setAffiliatedStock($this);
        }

        return $this;
    }

    public function removeInventoryDone(InventoryDone $inventoryDone): self
    {
        if ($this->inventoryDones->removeElement($inventoryDone)) {
            // set the owning side to null (unless already changed)
            if ($inventoryDone->getAffiliatedStock() === $this) {
                $inventoryDone->setAffiliatedStock(null);
            }
        }

        return $this;
    }
}
