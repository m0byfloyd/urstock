<?php

namespace App\Entity;

use App\Repository\RecipeInInventoryDoneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecipeInInventoryDoneRepository::class)]
class RecipeInInventoryDone
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: InventoryDone::class, cascade: ['persist', 'remove'], inversedBy: 'recipeInInventoryDones')]
    #[ORM\JoinColumn(nullable: false)]
    private $InventoryDone;

    #[ORM\ManyToOne(targetEntity: Recipe::class, inversedBy: 'recipeInInventoryDones')]
    #[ORM\JoinColumn(nullable: false)]
    private $Recipe;

    #[ORM\Column(type: 'integer')]
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecipe(): ?Recipe
    {
        return $this->Recipe;
    }

    public function setRecipe(?Recipe $Recipe): self
    {
        $this->Recipe = $Recipe;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getInventoryDone(): ArrayCollection
    {
        return $this->InventoryDone;
    }

    public function setInventoryDone(?InventoryDone $InventoryDone): self
    {
        $this->InventoryDone = $InventoryDone;

        return $this;
    }
}
