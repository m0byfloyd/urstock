<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    collectionOperations: [
        'post',
        'get',
        'collName_api_current_user_create_company' => [
            'route_name' => 'api_current_user_create_company'
        ],
        'collName_api_api_current_company_set_shipping_address' => [
            'route_name' => 'api_current_company_set_shipping_address'
        ],
        'collName_api_current_company' => [
            'route_name' => 'api_current_company'
        ],
        'collName_api_current_company_update' => [
            'route_name' => 'api_current_company_update'
        ],
    ]
)]

#[ORM\Entity(repositoryClass: CompanyRepository::class)]
class Company
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\OneToMany(mappedBy: 'company', targetEntity: User::class)]
    private $users;

    #[ORM\OneToOne(mappedBy: 'company', targetEntity: Stock::class, cascade: ['persist', 'remove'])]
    private $stock;

    #[ORM\Column(type: 'string', length: 14)]
    private $siret;

    #[ORM\Column(type: 'integer')]
    private $companySize;

    #[ORM\Column(type: 'string', length: 255)]
    private $email;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private $phoneNumber;

    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[ORM\OneToMany(mappedBy: 'Company', targetEntity: Recipe::class)]
    private $recipes;

    #[ORM\OneToMany(mappedBy: 'orderCompany', targetEntity: Order::class)]
    private $orders;

    #[ORM\OneToOne(mappedBy: 'CompanyAddress', targetEntity: ShippingAddress::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private $shippingAddress;

    #[ORM\OneToOne(mappedBy: 'Company', targetEntity: CompanyLastVendorsManager::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private $companyLastVendors;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->recipes = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCompany($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCompany() === $this) {
                $user->setCompany(null);
            }
        }

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setStock(Stock $stock): self
    {
        // set the owning side of the relation if necessary
        if ($stock->getCompany() !== $this) {
            $stock->setCompany($this);
        }

        $this->stock = $stock;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getCompanySize(): ?int
    {
        return $this->companySize;
    }

    public function setCompanySize(int $companySize): self
    {
        $this->companySize = $companySize;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection<int, Recipe>
     */
    public function getRecipes(): Collection
    {
        return $this->recipes;
    }

    public function addRecipe(Recipe $recipe): self
    {
        if (!$this->recipes->contains($recipe)) {
            $this->recipes[] = $recipe;
            $recipe->setCompany($this);
        }

        return $this;
    }

    public function removeRecipe(Recipe $recipe): self
    {
        if ($this->recipes->removeElement($recipe)) {
            // set the owning side to null (unless already changed)
            if ($recipe->getCompany() === $this) {
                $recipe->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setOrderCompany($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getOrderCompany() === $this) {
                $order->setOrderCompany(null);
            }
        }

        return $this;
    }

    public function getShippingAddress(): ?ShippingAddress
    {
        return $this->shippingAddress;
    }

    public function setShippingAddress(?ShippingAddress $shippingAddress): self
    {
        // unset the owning side of the relation if necessary
        if ($shippingAddress === null && $this->shippingAddress !== null) {
            $this->shippingAddress->setCompanyAddress(null);
        }

        // set the owning side of the relation if necessary
        if ($shippingAddress !== null && $shippingAddress->getCompanyAddress() !== $this) {
            $shippingAddress->setCompanyAddress($this);
        }

        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    public function getCompanyLastVendors(): ?CompanyLastVendorsManager
    {
        return $this->companyLastVendors;
    }

    public function setCompanyLastVendors(?CompanyLastVendorsManager $companyLastVendors): self
    {
        // unset the owning side of the relation if necessary
        if ($companyLastVendors === null && $this->companyLastVendors !== null) {
            $this->companyLastVendors->setCompany(null);
        }

        // set the owning side of the relation if necessary
        if ($companyLastVendors !== null && $companyLastVendors->getCompany() !== $this) {
            $companyLastVendors->setCompany($this);
        }

        $this->companyLastVendors = $companyLastVendors;

        return $this;
    }
}
