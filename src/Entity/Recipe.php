<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RecipeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    collectionOperations: [
        'collName_api_current_company_recipes' => [
            'route_name' => 'api_current_company_recipes',
        ],
        'collName_api_create_recipe' => [
            'route_name' => 'api_create_recipe',
            'method' => 'POST'
        ],
    ],
    normalizationContext: ['groups' => ['recipeDetail']]
)]
#[ORM\Entity(repositoryClass: RecipeRepository::class)]
class Recipe
{
    #[Groups(['recipeDetail'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Company::class, inversedBy: 'recipes')]
    private $Company;

    #[Groups(['recipeDetail'])]
    #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: ProductInRecipe::class, orphanRemoval: true)]
    private $ProductsInRecipe;

    #[Groups(['recipeDetail'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $recipeName;

    #[Groups(['recipeDetail'])]
    #[ORM\Column(type: 'integer')]
    private $price;

    #[ORM\OneToMany(mappedBy: 'Recipe', targetEntity: RecipeInInventoryDone::class)]
    private $recipeInInventoryDones;

    public function __construct()
    {
        $this->ProductsInRecipe = new ArrayCollection();
        $this->recipeInInventoryDones = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getCompany(): ?Company
    {
        return $this->Company;
    }
    public function setCompany(?Company $Company): self
    {
        $this->Company = $Company;

        return $this;
    }
    /**
     * @return Collection<int, ProductInRecipe>
     */
    public function getProductsInRecipe(): Collection
    {
        return $this->ProductsInRecipe;
    }
    public function addProductInRecipe(ProductInRecipe $productInRecipe): self
    {
        if (!$this->ProductsInRecipe->contains($productInRecipe)) {
            $this->ProductsInRecipe[] = $productInRecipe;
            $productInRecipe->setRecipe($this);
        }

        return $this;
    }
    public function removeProductInRecipe(ProductInRecipe $productInRecipe): self
    {
        if ($this->ProductsInRecipe->removeElement($productInRecipe)) {
            // set the owning side to null (unless already changed)
            if ($productInRecipe->getRecipe() === $this) {
                $productInRecipe->setRecipe(null);
            }
        }

        return $this;
    }
    public function getRecipeName(): ?string
    {
        return $this->recipeName;
    }
    public function setRecipeName(string $recipeName): self
    {
        $this->recipeName = $recipeName;

        return $this;
    }
    public function getPrice(): ?int
    {
        return $this->price;
    }
    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection<int, RecipeInInventoryDone>
     */
    public function getRecipeInInventoryDones(): Collection
    {
        return $this->recipeInInventoryDones;
    }

    public function addRecipeInInventoryDone(RecipeInInventoryDone $recipeInInventoryDone): self
    {
        if (!$this->recipeInInventoryDones->contains($recipeInInventoryDone)) {
            $this->recipeInInventoryDones[] = $recipeInInventoryDone;
            $recipeInInventoryDone->setRecipe($this);
        }

        return $this;
    }

    public function removeRecipeInInventoryDone(RecipeInInventoryDone $recipeInInventoryDone): self
    {
        if ($this->recipeInInventoryDones->removeElement($recipeInInventoryDone)) {
            // set the owning side to null (unless already changed)
            if ($recipeInInventoryDone->getRecipe() === $this) {
                $recipeInInventoryDone->setRecipe(null);
            }
        }

        return $this;
    }
}
