<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CompanyLastVendorsManagerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    collectionOperations: [
        'collName_api_current_company_last_vendors' => [
            'route_name' => 'api_current_company_shipping_last_vendors'
        ],
    ]
)]
#[ORM\Entity(repositoryClass: CompanyLastVendorsManagerRepository::class)]
class CompanyLastVendorsManager
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToOne(inversedBy: 'companyLastVendors', targetEntity: Company::class, cascade: ['persist', 'remove'])]
    private $Company;

    #[ORM\ManyToMany(targetEntity: Vendor::class, inversedBy: 'vendorLastCompanies')]
    private $Vendors;

    public function __construct()
    {
        $this->Vendors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->Company;
    }

    public function setCompany(?Company $Company): self
    {
        $this->Company = $Company;

        return $this;
    }

    /**
     * @return Collection<int, Vendor>
     */
    public function getVendors(): Collection
    {
        return $this->Vendors;
    }

    public function addVendor(Vendor $vendor): self
    {
        if (!$this->Vendors->contains($vendor)) {
            $this->Vendors[] = $vendor;
        }

        return $this;
    }

    public function removeVendor(Vendor $vendor): self
    {
        $this->Vendors->removeElement($vendor);

        return $this;
    }
}
