<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    collectionOperations: [
        'get'
    ],
    itemOperations: [
        'get'
    ],
    normalizationContext: ['groups' =>
        [
            'getProduct',
            'recipeDetail',
            'orderDetail'
        ]
    ]
)]

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[Groups(['orderDetail', 'recipeDetail'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['orderDetail', 'getProduct', 'recipeDetail'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $picture;

    #[Groups(['orderDetail', 'getProduct'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $description;

    #[Groups(['orderDetail', 'getProduct'])]
    #[ORM\Column(type: 'float')]
    private $weight;

    #[Groups(['orderDetail', 'getProduct'])]
    #[ORM\Column(type: 'string', length: 10)]
    private $measuringUnit;

    #[ORM\OneToMany(mappedBy: 'Product', targetEntity: ProductInStock::class, orphanRemoval: true)]
    private $productInStock;

    #[ORM\ManyToOne(targetEntity: Vendor::class, inversedBy: 'Products')]
    #[ORM\JoinColumn(nullable: false)]
    private $vendor;

    #[Groups(['orderDetail', 'getProduct'])]
    #[ORM\Column(type: 'integer')]
    private $price;

    #[Groups(['orderDetail', 'getProduct'])]
    private $quantityAvailable;

    #[Groups(['getProduct'])]
    #[ORM\ManyToOne(targetEntity: ProductSubCategory::class, inversedBy: 'Products')]
    private $productSubCategory;

    #[Groups(['orderDetail', 'getProduct'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $productName;

    #[ORM\OneToMany(mappedBy: 'Product', targetEntity: ProductInOrder::class, cascade: ['persist', 'remove'])]
    private $productInOrder;

    #[Groups(['orderDetail', 'getProduct'])]
    #[ORM\Column(type: 'string', length: 10)]
    private $reference;

    #[ORM\OneToMany(mappedBy: 'Product', targetEntity: ProductInInventoryDone::class)]
    private $productInInventoryDone;

    public function __construct()
    {
        $this->stocks = new ArrayCollection();
        $this->productInStock = new ArrayCollection();
        $this->productInInventoryDone = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getMeasuringUnit(): ?string
    {
        return $this->measuringUnit;
    }

    public function setMeasuringUnit(string $measuringUnit): self
    {
        $this->measuringUnit = $measuringUnit;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProductInStock(): Collection
    {
        return $this->productInStock;
    }

    public function addProductInStock(ProductInStock $ProductInStock): self
    {
        if (!$this->productInStock->contains($ProductInStock)) {
            $this->productInStock[] = $ProductInStock;
            $ProductInStock->setProduct($this);
        }

        return $this;
    }

    public function removeProductInStock(ProductInStock $ProductInStock): self
    {
        if ($this->productInStock->removeElement($ProductInStock)) {
            // set the owning side to null (unless already changed)
            if ($ProductInStock->getProduct() === $this) {
                $ProductInStock->setProduct(null);
            }
        }

        return $this;
    }

    public function getVendor(): ?Vendor
    {
        return $this->vendor;
    }

    public function setVendor(?Vendor $vendor): self
    {
        $this->vendor = $vendor;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantityAvailable(): ?int
    {
        return $this->quantityAvailable;
    }

    public function setQuantityAvailable(int $quantityAvailable): self
    {
        $this->quantityAvailable = $quantityAvailable;

        return $this;
    }

    public function getProductSubCategory(): ?ProductSubCategory
    {
        return $this->productSubCategory;
    }

    public function setProductSubCategory(?ProductSubCategory $productSubCategory): self
    {
        $this->productSubCategory = $productSubCategory;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->productName;
    }

    public function setProductName(string $productName): self
    {
        $this->productName = $productName;

        return $this;
    }

    public function getProductInOrder(): ?ProductInOrder
    {
        return $this->productInOrder;
    }

    public function addProductInOrder(ProductInOrder $ProductInOrder): self
    {
        // set the owning side of the relation if necessary
        if (!$this->productInOrder->contains($ProductInOrder)) {
            $this->productInOrder[] = $ProductInOrder;
            $ProductInOrder->setProduct($this);
        }

        return $this;
    }

    public function removeProductInOrder(ProductInStock $ProductInOrder): self
    {
        if ($this->productInOrder->removeElement($ProductInOrder)) {
            // set the owning side to null (unless already changed)
            if ($ProductInOrder->getProduct() === $this) {
                $ProductInOrder->setProduct(null);
            }
        }

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return Collection<int, ProductInInventoryDone>
     */
    public function getProductInInventoryDone(): Collection
    {
        return $this->productInInventoryDone;
    }

    public function addProductInInventoryDone(ProductInInventoryDone $productInInventoryDone): self
    {
        if (!$this->productInInventoryDone->contains($productInInventoryDone)) {
            $this->productInInventoryDone[] = $productInInventoryDone;
            $productInInventoryDone->setProduct($this);
        }

        return $this;
    }

    public function removeProductInInventoryDone(ProductInInventoryDone $productInInventoryDone): self
    {
        if ($this->productInInventoryDone->removeElement($productInInventoryDone)) {
            // set the owning side to null (unless already changed)
            if ($productInInventoryDone->getProduct() === $this) {
                $productInInventoryDone->setProduct(null);
            }
        }

        return $this;
    }
}
