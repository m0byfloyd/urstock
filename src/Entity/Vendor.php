<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\VendorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource]
#[ORM\Entity(repositoryClass: VendorRepository::class)]
class Vendor
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups("orderDetail")]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("orderDetail")]
    private $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $address;

    #[ORM\OneToMany(targetEntity: Product::class, mappedBy: 'vendor', orphanRemoval: true)]
    private $Products;

    #[ORM\OneToMany(targetEntity: User::class, mappedBy: 'vendor')]
    private $UserAccount;

    #[ORM\ManyToMany(targetEntity: CompanyLastVendorsManager::class, mappedBy: 'Vendors')]
    private $vendorLastCompanies;

    public function __construct()
    {
        $this->Products = new ArrayCollection();
        $this->UserAccount = new ArrayCollection();
        $this->vendorLastCompanies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->Products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->Products->contains($product)) {
            $this->Products[] = $product;
            $product->setVendor($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->Products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getVendor() === $this) {
                $product->setVendor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUserAccount(): Collection
    {
        return $this->UserAccount;
    }

    public function addUserAccount(User $userAccount): self
    {
        if (!$this->UserAccount->contains($userAccount)) {
            $this->UserAccount[] = $userAccount;
            $userAccount->setVendor($this);
        }

        return $this;
    }

    public function removeUserAccount(User $userAccount): self
    {
        if ($this->UserAccount->removeElement($userAccount)) {
            // set the owning side to null (unless already changed)
            if ($userAccount->getVendor() === $this) {
                $userAccount->setVendor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CompanyLastVendorsManager>
     */
    public function getVendorLastCompanies(): Collection
    {
        return $this->vendorLastCompanies;
    }

    public function addVendorLastCompany(CompanyLastVendorsManager $vendorLastCompany): self
    {
        if (!$this->vendorLastCompanies->contains($vendorLastCompany)) {
            $this->vendorLastCompanies[] = $vendorLastCompany;
            $vendorLastCompany->addVendor($this);
        }

        return $this;
    }

    public function removeVendorLastCompany(CompanyLastVendorsManager $vendorLastCompany): self
    {
        if ($this->vendorLastCompanies->removeElement($vendorLastCompany)) {
            $vendorLastCompany->removeVendor($this);
        }

        return $this;
    }
}
