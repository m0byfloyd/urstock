<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductInStockRepository;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    collectionOperations: [
        'post',
        'get',
        'collName_api_current_company_stock_add_product' => [
            'route_name' => 'api_current_company_stock_add_product'
        ],
        'collName_api_current_company_stock_delete_product' => [
            'route_name' => 'api_current_company_stock_delete_product'
        ],
        'collName_api_current_company_stock_update_product_quantity' => [
            'route_name' => 'api_current_company_stock_update_product_quantity'
        ]
    ]
)]
#[ORM\Entity(repositoryClass: ProductInStockRepository::class)]
class ProductInStock
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'productInStock')]
    #[ORM\JoinColumn(nullable: false)]
    private $Product;

    #[ORM\Column(type: 'integer')]
    private $quantity;

    #[ORM\ManyToOne(targetEntity: Stock::class, inversedBy: 'productInStock')]
    #[ORM\JoinColumn(nullable: false)]
    private $Stock;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->Product;
    }

    public function setProduct(?Product $Product): self
    {
        $this->Product = $Product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->Stock;
    }

    public function setStock(?Stock $Stock): self
    {
        $this->Stock = $Stock;

        return $this;
    }
}
