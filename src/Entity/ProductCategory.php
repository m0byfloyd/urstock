<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource]
#[ORM\Entity(repositoryClass: ProductCategoryRepository::class)]
class ProductCategory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\OneToMany(mappedBy: 'productCategory', targetEntity: ProductSubCategory::class)]
    private $SubCategories;

    public function __construct()
    {
        $this->SubCategories = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getName(): ?string
    {
        return $this->name;
    }
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    /**
     * @return Collection<int, ProductSubCategory>
     */
    public function getSubCategories(): Collection
    {
        return $this->SubCategories;
    }
    public function addSubCategory(ProductSubCategory $subCategory): self
    {
        if (!$this->SubCategories->contains($subCategory)) {
            $this->SubCategories[] = $subCategory;
            $subCategory->setProductCategory($this);
        }

        return $this;
    }
    public function removeSubCategory(ProductSubCategory $subCategory): self
    {
        if ($this->SubCategories->removeElement($subCategory)) {
            // set the owning side to null (unless already changed)
            if ($subCategory->getProductCategory() === $this) {
                $subCategory->setProductCategory(null);
            }
        }

        return $this;
    }
}
