<?php

namespace App\Entity;

use App\Repository\ProductInInventoryDoneRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductInInventoryDoneRepository::class)]
class ProductInInventoryDone
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: InventoryDone::class, cascade: ['persist', 'remove'], inversedBy: 'productInInventoryDones')]
    #[ORM\JoinColumn(nullable: false)]
    private $InventoryDone;

    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'productInInventoryDone')]
    #[ORM\JoinColumn(nullable: false)]
    private $Product;

    #[ORM\Column(type: 'integer')]
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInventoryDone(): ?InventoryDone
    {
        return $this->InventoryDone;
    }

    public function setInventoryDone(?InventoryDone $InventoryDone): self
    {
        $this->InventoryDone = $InventoryDone;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->Product;
    }

    public function setProduct(?Product $Product): self
    {
        $this->Product = $Product;

        return $this;
    }
}
