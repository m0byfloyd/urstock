<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductInRecipeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    normalizationContext: ['groups' => ['recipeDetail']]
)]
#[ORM\Entity(repositoryClass: ProductInRecipeRepository::class)]
class ProductInRecipe
{
    #[Groups(['recipeDetail'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['recipeDetail'])]
    #[ORM\ManyToOne(targetEntity: Product::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private $Ingredient;

    #[ORM\ManyToOne(targetEntity: Recipe::class, inversedBy: 'ProductsInRecipe')]
    #[ORM\JoinColumn(nullable: false)]
    private $recipe;

    #[Groups(['recipeDetail'])]
    #[ORM\Column(type: 'integer')]
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getIngredient(): ?Product
    {
        return $this->Ingredient;
    }
    public function setIngredient(Product $Ingredient): self
    {
        $this->Ingredient = $Ingredient;

        return $this;
    }
    public function getRecipe(): ?Recipe
    {
        return $this->recipe;
    }
    public function setRecipe(?Recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
