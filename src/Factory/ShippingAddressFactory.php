<?php

namespace App\Factory;

use App\Entity\ShippingAddress;
use App\Repository\ShippingAddressRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ShippingAddress>
 *
 * @method static ShippingAddress|Proxy createOne(array $attributes = [])
 * @method static ShippingAddress[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ShippingAddress|Proxy find(object|array|mixed $criteria)
 * @method static ShippingAddress|Proxy findOrCreate(array $attributes)
 * @method static ShippingAddress|Proxy first(string $sortedField = 'id')
 * @method static ShippingAddress|Proxy last(string $sortedField = 'id')
 * @method static ShippingAddress|Proxy random(array $attributes = [])
 * @method static ShippingAddress|Proxy randomOrCreate(array $attributes = [])
 * @method static ShippingAddress[]|Proxy[] all()
 * @method static ShippingAddress[]|Proxy[] findBy(array $attributes)
 * @method static ShippingAddress[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ShippingAddress[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ShippingAddressRepository|RepositoryProxy repository()
 * @method ShippingAddress|Proxy create(array|callable $attributes = [])
 */
final class ShippingAddressFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'city' => self::faker()->text(),
            'zipcode' => self::faker()->text(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(ShippingAddress $shippingAddress): void {})
        ;
    }

    protected static function getClass(): string
    {
        return ShippingAddress::class;
    }
}
