<?php

namespace App\Factory;

use App\Entity\ProductInRecipe;
use App\Repository\ProductInRecipeRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ProductInRecipe>
 *
 * @method static ProductInRecipe|Proxy createOne(array $attributes = [])
 * @method static ProductInRecipe[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ProductInRecipe|Proxy find(object|array|mixed $criteria)
 * @method static ProductInRecipe|Proxy findOrCreate(array $attributes)
 * @method static ProductInRecipe|Proxy first(string $sortedField = 'id')
 * @method static ProductInRecipe|Proxy last(string $sortedField = 'id')
 * @method static ProductInRecipe|Proxy random(array $attributes = [])
 * @method static ProductInRecipe|Proxy randomOrCreate(array $attributes = [])
 * @method static ProductInRecipe[]|Proxy[] all()
 * @method static ProductInRecipe[]|Proxy[] findBy(array $attributes)
 * @method static ProductInRecipe[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ProductInRecipe[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ProductInRecipeRepository|RepositoryProxy repository()
 * @method ProductInRecipe|Proxy create(array|callable $attributes = [])
 */
final class ProductInRecipeFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(ProductInRecipe $productInRecipe): void {})
        ;
    }

    protected static function getClass(): string
    {
        return ProductInRecipe::class;
    }
}
