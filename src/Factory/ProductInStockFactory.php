<?php

namespace App\Factory;

use App\Entity\ProductInStock;
use App\Repository\ProductInStockRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ProductInStock>
 *
 * @method static ProductInStock|Proxy createOne(array $attributes = [])
 * @method static ProductInStock[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ProductInStock|Proxy find(object|array|mixed $criteria)
 * @method static ProductInStock|Proxy findOrCreate(array $attributes)
 * @method static ProductInStock|Proxy first(string $sortedField = 'id')
 * @method static ProductInStock|Proxy last(string $sortedField = 'id')
 * @method static ProductInStock|Proxy random(array $attributes = [])
 * @method static ProductInStock|Proxy randomOrCreate(array $attributes = [])
 * @method static ProductInStock[]|Proxy[] all()
 * @method static ProductInStock[]|Proxy[] findBy(array $attributes)
 * @method static ProductInStock[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ProductInStock[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ProductInStockRepository|RepositoryProxy repository()
 * @method ProductInStock|Proxy create(array|callable $attributes = [])
 */
final class ProductInStockFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'quantity' => self::faker()->randomNumber(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(ProductInStock $productInStock): void {})
        ;
    }

    protected static function getClass(): string
    {
        return ProductInStock::class;
    }
}
