<?php

namespace App\Factory;

use App\Entity\ProductCategory;
use App\Repository\ProductCategoryRepository;
use JetBrains\PhpStorm\ArrayShape;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ProductCategory>
 *
 * @method static ProductCategory|Proxy createOne(array $attributes = [])
 * @method static ProductCategory[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ProductCategory|Proxy find(object|array|mixed $criteria)
 * @method static ProductCategory|Proxy findOrCreate(array $attributes)
 * @method static ProductCategory|Proxy first(string $sortedField = 'id')
 * @method static ProductCategory|Proxy last(string $sortedField = 'id')
 * @method static ProductCategory|Proxy random(array $attributes = [])
 * @method static ProductCategory|Proxy randomOrCreate(array $attributes = [])
 * @method static ProductCategory[]|Proxy[] all()
 * @method static ProductCategory[]|Proxy[] findBy(array $attributes)
 * @method static ProductCategory[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ProductCategory[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ProductCategoryRepository|RepositoryProxy repository()
 * @method ProductCategory|Proxy create(array|callable $attributes = [])
 */
final class ProductCategoryFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    #[ArrayShape(['name' => "array|string"])] protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'name' => self::faker()->word(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(ProductCategory $productCategory): void {})
        ;
    }

    protected static function getClass(): string
    {
        return ProductCategory::class;
    }
}
