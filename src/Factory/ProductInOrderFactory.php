<?php

namespace App\Factory;

use App\Entity\ProductInOrder;
use App\Repository\ProductInOrderRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ProductInOrder>
 *
 * @method static ProductInOrder|Proxy createOne(array $attributes = [])
 * @method static ProductInOrder[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ProductInOrder|Proxy find(object|array|mixed $criteria)
 * @method static ProductInOrder|Proxy findOrCreate(array $attributes)
 * @method static ProductInOrder|Proxy first(string $sortedField = 'id')
 * @method static ProductInOrder|Proxy last(string $sortedField = 'id')
 * @method static ProductInOrder|Proxy random(array $attributes = [])
 * @method static ProductInOrder|Proxy randomOrCreate(array $attributes = [])
 * @method static ProductInOrder[]|Proxy[] all()
 * @method static ProductInOrder[]|Proxy[] findBy(array $attributes)
 * @method static ProductInOrder[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ProductInOrder[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ProductInOrderRepository|RepositoryProxy repository()
 * @method ProductInOrder|Proxy create(array|callable $attributes = [])
 */
final class ProductInOrderFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(ProductInOrder $productInOrder): void {})
        ;
    }

    protected static function getClass(): string
    {
        return ProductInOrder::class;
    }
}
