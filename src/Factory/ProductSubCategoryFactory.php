<?php

namespace App\Factory;

use App\Entity\ProductSubCategory;
use App\Repository\ProductSubCategoryRepository;
use JetBrains\PhpStorm\ArrayShape;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ProductSubCategory>
 *
 * @method static ProductSubCategory|Proxy createOne(array $attributes = [])
 * @method static ProductSubCategory[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ProductSubCategory|Proxy find(object|array|mixed $criteria)
 * @method static ProductSubCategory|Proxy findOrCreate(array $attributes)
 * @method static ProductSubCategory|Proxy first(string $sortedField = 'id')
 * @method static ProductSubCategory|Proxy last(string $sortedField = 'id')
 * @method static ProductSubCategory|Proxy random(array $attributes = [])
 * @method static ProductSubCategory|Proxy randomOrCreate(array $attributes = [])
 * @method static ProductSubCategory[]|Proxy[] all()
 * @method static ProductSubCategory[]|Proxy[] findBy(array $attributes)
 * @method static ProductSubCategory[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ProductSubCategory[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ProductSubCategoryRepository|RepositoryProxy repository()
 * @method ProductSubCategory|Proxy create(array|callable $attributes = [])
 */
final class ProductSubCategoryFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    #[ArrayShape(['name' => "array|string"])] protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'name' => self::faker()->word(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(ProductSubCategory $productSubCategory): void {})
        ;
    }

    protected static function getClass(): string
    {
        return ProductSubCategory::class;
    }
}
