--
-- PostgreSQL database dump
--

-- Dumped from database version 13.7
-- Dumped by pg_dump version 13.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: company; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.company (
    id integer NOT NULL,
    shipping_address_id integer,
    company_last_vendors_id integer,
    name character varying(255) NOT NULL,
    siret character varying(14) NOT NULL,
    company_size integer NOT NULL,
    email character varying(255) NOT NULL,
    phone_number character varying(20) DEFAULT NULL::character varying,
    created_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.company OWNER TO symfony;

--
-- Name: company_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_id_seq OWNER TO symfony;

--
-- Name: company_last_vendors_manager; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.company_last_vendors_manager (
    id integer NOT NULL,
    company_id integer
);


ALTER TABLE public.company_last_vendors_manager OWNER TO symfony;

--
-- Name: company_last_vendors_manager_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.company_last_vendors_manager_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_last_vendors_manager_id_seq OWNER TO symfony;

--
-- Name: company_last_vendors_manager_vendor; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.company_last_vendors_manager_vendor (
    company_last_vendors_manager_id integer NOT NULL,
    vendor_id integer NOT NULL
);


ALTER TABLE public.company_last_vendors_manager_vendor OWNER TO symfony;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO symfony;

--
-- Name: inventory_done; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.inventory_done (
    id integer NOT NULL,
    affiliated_stock_id integer NOT NULL,
    author_id integer NOT NULL,
    date timestamp(0) without time zone NOT NULL,
    note text,
    revenues integer NOT NULL
);


ALTER TABLE public.inventory_done OWNER TO symfony;

--
-- Name: inventory_done_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.inventory_done_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inventory_done_id_seq OWNER TO symfony;

--
-- Name: order; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public."order" (
    id integer NOT NULL,
    order_company_id integer NOT NULL,
    status character varying(20) NOT NULL,
    price integer NOT NULL,
    date date NOT NULL
);


ALTER TABLE public."order" OWNER TO symfony;

--
-- Name: order_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_id_seq OWNER TO symfony;

--
-- Name: product; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.product (
    id integer NOT NULL,
    vendor_id integer NOT NULL,
    product_sub_category_id integer,
    picture character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    weight double precision NOT NULL,
    measuring_unit character varying(10) NOT NULL,
    price integer NOT NULL,
    product_name character varying(255) NOT NULL,
    reference character varying(10) NOT NULL
);


ALTER TABLE public.product OWNER TO symfony;

--
-- Name: product_category; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.product_category (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.product_category OWNER TO symfony;

--
-- Name: product_category_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.product_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_category_id_seq OWNER TO symfony;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO symfony;

--
-- Name: product_in_inventory_done; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.product_in_inventory_done (
    id integer NOT NULL,
    inventory_done_id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE public.product_in_inventory_done OWNER TO symfony;

--
-- Name: product_in_inventory_done_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.product_in_inventory_done_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_in_inventory_done_id_seq OWNER TO symfony;

--
-- Name: product_in_order; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.product_in_order (
    id integer NOT NULL,
    order_entity_id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE public.product_in_order OWNER TO symfony;

--
-- Name: product_in_order_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.product_in_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_in_order_id_seq OWNER TO symfony;

--
-- Name: product_in_recipe; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.product_in_recipe (
    id integer NOT NULL,
    ingredient_id integer NOT NULL,
    recipe_id integer NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE public.product_in_recipe OWNER TO symfony;

--
-- Name: product_in_recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.product_in_recipe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_in_recipe_id_seq OWNER TO symfony;

--
-- Name: product_in_stock; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.product_in_stock (
    id integer NOT NULL,
    product_id integer NOT NULL,
    stock_id integer NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE public.product_in_stock OWNER TO symfony;

--
-- Name: product_in_stock_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.product_in_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_in_stock_id_seq OWNER TO symfony;

--
-- Name: product_sub_category; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.product_sub_category (
    id integer NOT NULL,
    product_category_id integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.product_sub_category OWNER TO symfony;

--
-- Name: product_sub_category_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.product_sub_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_sub_category_id_seq OWNER TO symfony;

--
-- Name: recipe; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.recipe (
    id integer NOT NULL,
    company_id integer,
    recipe_name character varying(255) NOT NULL,
    price integer NOT NULL
);


ALTER TABLE public.recipe OWNER TO symfony;

--
-- Name: recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.recipe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipe_id_seq OWNER TO symfony;

--
-- Name: recipe_in_inventory_done; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.recipe_in_inventory_done (
    id integer NOT NULL,
    inventory_done_id integer NOT NULL,
    recipe_id integer NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE public.recipe_in_inventory_done OWNER TO symfony;

--
-- Name: recipe_in_inventory_done_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.recipe_in_inventory_done_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipe_in_inventory_done_id_seq OWNER TO symfony;

--
-- Name: shipping_address; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.shipping_address (
    id integer NOT NULL,
    company_address_id integer,
    city character varying(60) NOT NULL,
    address character varying(100) NOT NULL,
    zipcode character varying(5) NOT NULL,
    complement_of_address character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE public.shipping_address OWNER TO symfony;

--
-- Name: shipping_address_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.shipping_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shipping_address_id_seq OWNER TO symfony;

--
-- Name: stock; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.stock (
    id integer NOT NULL,
    company_id integer NOT NULL
);


ALTER TABLE public.stock OWNER TO symfony;

--
-- Name: stock_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_id_seq OWNER TO symfony;

--
-- Name: user; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    company_id integer,
    vendor_id integer,
    email character varying(180) NOT NULL,
    roles json NOT NULL,
    password character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    firstname character varying(255) NOT NULL,
    phone_number character varying(20) DEFAULT NULL::character varying,
    created_at timestamp(0) without time zone NOT NULL,
    is_verified boolean NOT NULL
);


ALTER TABLE public."user" OWNER TO symfony;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO symfony;

--
-- Name: vendor; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.vendor (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    address character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.vendor OWNER TO symfony;

--
-- Name: vendor_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.vendor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vendor_id_seq OWNER TO symfony;

--
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.company VALUES (1, 1, 1, 'La boulangerie sûre', 'Distinctio.', 1, 'boulangeriesure@gmail.com', NULL, '2022-07-17 17:25:24');


--
-- Data for Name: company_last_vendors_manager; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.company_last_vendors_manager VALUES (1, 1);


--
-- Data for Name: company_last_vendors_manager_vendor; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.company_last_vendors_manager_vendor VALUES (1, 1);


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.doctrine_migration_versions VALUES ('DoctrineMigrations\Version20220717172507', '2022-07-17 17:25:18', 300);


--
-- Data for Name: inventory_done; Type: TABLE DATA; Schema: public; Owner: symfony
--



--
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public."order" VALUES (1, 1, 'pending', 500, '2022-07-17');


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.product VALUES (1, 1, 2, NULL, 'Farine de qualité supérieure', 30.5, 'kg', 1000, 'Farine ++', 'XJWJQH96SX');
INSERT INTO public.product VALUES (2, 1, 2, NULL, 'Notre célèbre levure', 0.11, 'kg', 1500, 'Levure Superbake', 'UJ10MBUQ05');
INSERT INTO public.product VALUES (3, 1, 2, NULL, 'Sel fin provenant de Bretagne', 26517989.7, 'kg', 10, 'Sel fin', 'NJ6CWO1KB7');


--
-- Data for Name: product_category; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.product_category VALUES (1, 'La boulangerie');


--
-- Data for Name: product_in_inventory_done; Type: TABLE DATA; Schema: public; Owner: symfony
--



--
-- Data for Name: product_in_order; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.product_in_order VALUES (1, 1, 1, 320);
INSERT INTO public.product_in_order VALUES (2, 1, 3, 5);
INSERT INTO public.product_in_order VALUES (3, 1, 2, 8);


--
-- Data for Name: product_in_recipe; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.product_in_recipe VALUES (1, 1, 1, 320);
INSERT INTO public.product_in_recipe VALUES (2, 3, 1, 5);
INSERT INTO public.product_in_recipe VALUES (3, 2, 1, 8);


--
-- Data for Name: product_in_stock; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.product_in_stock VALUES (1, 2, 1, 6);
INSERT INTO public.product_in_stock VALUES (2, 3, 1, 81501);
INSERT INTO public.product_in_stock VALUES (3, 1, 1, 99);


--
-- Data for Name: product_sub_category; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.product_sub_category VALUES (1, 1, 'Les indispensables');
INSERT INTO public.product_sub_category VALUES (2, 1, 'Les extras');


--
-- Data for Name: recipe; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.recipe VALUES (1, 1, 'Pain', 500);


--
-- Data for Name: recipe_in_inventory_done; Type: TABLE DATA; Schema: public; Owner: symfony
--



--
-- Data for Name: shipping_address; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.shipping_address VALUES (1, 1, 'Troyes', '12 rue de la Dîme', '46780', NULL);


--
-- Data for Name: stock; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.stock VALUES (1, 1);


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public."user" VALUES (1, NULL, NULL, 'eyeinthesky@sirius.com', '["ROLE_ADMIN","ROLE_USER"]', '$2y$10$QLjBifXk9NbwkCKzuznQG..PADBxnGQ5AAWDevMI.J/yoc4C9ua8C', 'Dumas', 'Michel', '', '2022-07-17 17:25:24', false);
INSERT INTO public."user" VALUES (2, NULL, NULL, 'john@doe.com', '["ROLE_USER"]', '$2y$10$QLjBifXk9NbwkCKzuznQG..PADBxnGQ5AAWDevMI.J/yoc4C9ua8C', 'Doe', 'John', '', '2022-07-17 17:25:24', false);
INSERT INTO public."user" VALUES (3, NULL, 1, 'fake.bake@urs.com', '["ROLE_USER","ROLE_VENDOR"]', '$2a$12$gk3c0OQ9F0I3m.QeFxwS7um2XCMdb/Xrpe9IVb07HmV/Bbbc/xtGq', 'Maria', 'Thibault', '', '2022-07-17 17:25:24', false);
INSERT INTO public."user" VALUES (4, 1, NULL, 'fake.bakery@urs.com', '["ROLE_USER","ROLE_VENDOR"]', '$2a$12$gk3c0OQ9F0I3m.QeFxwS7um2XCMdb/Xrpe9IVb07HmV/Bbbc/xtGq', 'Stanislas', 'Jean', '', '2022-07-17 17:25:24', false);


--
-- Data for Name: vendor; Type: TABLE DATA; Schema: public; Owner: symfony
--

INSERT INTO public.vendor VALUES (1, 'Super Bake', '10 rue du Poitou, Lille');


--
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.company_id_seq', 1, true);


--
-- Name: company_last_vendors_manager_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.company_last_vendors_manager_id_seq', 1, true);


--
-- Name: inventory_done_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.inventory_done_id_seq', 1, false);


--
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.order_id_seq', 1, true);


--
-- Name: product_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.product_category_id_seq', 1, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.product_id_seq', 3, true);


--
-- Name: product_in_inventory_done_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.product_in_inventory_done_id_seq', 1, false);


--
-- Name: product_in_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.product_in_order_id_seq', 3, true);


--
-- Name: product_in_recipe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.product_in_recipe_id_seq', 3, true);


--
-- Name: product_in_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.product_in_stock_id_seq', 3, true);


--
-- Name: product_sub_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.product_sub_category_id_seq', 2, true);


--
-- Name: recipe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.recipe_id_seq', 1, true);


--
-- Name: recipe_in_inventory_done_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.recipe_in_inventory_done_id_seq', 1, false);


--
-- Name: shipping_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.shipping_address_id_seq', 1, true);


--
-- Name: stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.stock_id_seq', 1, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.user_id_seq', 4, true);


--
-- Name: vendor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.vendor_id_seq', 1, true);


--
-- Name: company_last_vendors_manager company_last_vendors_manager_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.company_last_vendors_manager
    ADD CONSTRAINT company_last_vendors_manager_pkey PRIMARY KEY (id);


--
-- Name: company_last_vendors_manager_vendor company_last_vendors_manager_vendor_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.company_last_vendors_manager_vendor
    ADD CONSTRAINT company_last_vendors_manager_vendor_pkey PRIMARY KEY (company_last_vendors_manager_id, vendor_id);


--
-- Name: company company_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: inventory_done inventory_done_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.inventory_done
    ADD CONSTRAINT inventory_done_pkey PRIMARY KEY (id);


--
-- Name: order order_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- Name: product_category product_category_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_pkey PRIMARY KEY (id);


--
-- Name: product_in_inventory_done product_in_inventory_done_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_inventory_done
    ADD CONSTRAINT product_in_inventory_done_pkey PRIMARY KEY (id);


--
-- Name: product_in_order product_in_order_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_order
    ADD CONSTRAINT product_in_order_pkey PRIMARY KEY (id);


--
-- Name: product_in_recipe product_in_recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_recipe
    ADD CONSTRAINT product_in_recipe_pkey PRIMARY KEY (id);


--
-- Name: product_in_stock product_in_stock_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_stock
    ADD CONSTRAINT product_in_stock_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: product_sub_category product_sub_category_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_sub_category
    ADD CONSTRAINT product_sub_category_pkey PRIMARY KEY (id);


--
-- Name: recipe_in_inventory_done recipe_in_inventory_done_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.recipe_in_inventory_done
    ADD CONSTRAINT recipe_in_inventory_done_pkey PRIMARY KEY (id);


--
-- Name: recipe recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (id);


--
-- Name: shipping_address shipping_address_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.shipping_address
    ADD CONSTRAINT shipping_address_pkey PRIMARY KEY (id);


--
-- Name: stock stock_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.stock
    ADD CONSTRAINT stock_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: vendor vendor_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.vendor
    ADD CONSTRAINT vendor_pkey PRIMARY KEY (id);


--
-- Name: idx_27db1afc4584665a; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_27db1afc4584665a ON public.product_in_inventory_done USING btree (product_id);


--
-- Name: idx_27db1afce9c97ec4; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_27db1afce9c97ec4 ON public.product_in_inventory_done USING btree (inventory_done_id);


--
-- Name: idx_3147d5f3be6903fd; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_3147d5f3be6903fd ON public.product_sub_category USING btree (product_category_id);


--
-- Name: idx_3dfae9f59d8a214; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_3dfae9f59d8a214 ON public.product_in_recipe USING btree (recipe_id);


--
-- Name: idx_3dfae9f933fe08c; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_3dfae9f933fe08c ON public.product_in_recipe USING btree (ingredient_id);


--
-- Name: idx_4117c00b59d8a214; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_4117c00b59d8a214 ON public.recipe_in_inventory_done USING btree (recipe_id);


--
-- Name: idx_4117c00be9c97ec4; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_4117c00be9c97ec4 ON public.recipe_in_inventory_done USING btree (inventory_done_id);


--
-- Name: idx_5a8b2f8552d84f11; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_5a8b2f8552d84f11 ON public.inventory_done USING btree (affiliated_stock_id);


--
-- Name: idx_5a8b2f85f675f31b; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_5a8b2f85f675f31b ON public.inventory_done USING btree (author_id);


--
-- Name: idx_746ce7713da206a5; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_746ce7713da206a5 ON public.product_in_order USING btree (order_entity_id);


--
-- Name: idx_746ce7714584665a; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_746ce7714584665a ON public.product_in_order USING btree (product_id);


--
-- Name: idx_8d93d649979b1ad6; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_8d93d649979b1ad6 ON public."user" USING btree (company_id);


--
-- Name: idx_8d93d649f603ee73; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_8d93d649f603ee73 ON public."user" USING btree (vendor_id);


--
-- Name: idx_ca7322894584665a; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_ca7322894584665a ON public.product_in_stock USING btree (product_id);


--
-- Name: idx_ca732289dcd6110; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_ca732289dcd6110 ON public.product_in_stock USING btree (stock_id);


--
-- Name: idx_d34a04adc534ede1; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_d34a04adc534ede1 ON public.product USING btree (product_sub_category_id);


--
-- Name: idx_d34a04adf603ee73; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_d34a04adf603ee73 ON public.product USING btree (vendor_id);


--
-- Name: idx_da88b137979b1ad6; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_da88b137979b1ad6 ON public.recipe USING btree (company_id);


--
-- Name: idx_f3b802926288684f; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_f3b802926288684f ON public.company_last_vendors_manager_vendor USING btree (company_last_vendors_manager_id);


--
-- Name: idx_f3b80292f603ee73; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_f3b80292f603ee73 ON public.company_last_vendors_manager_vendor USING btree (vendor_id);


--
-- Name: idx_f52993982441e783; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_f52993982441e783 ON public."order" USING btree (order_company_id);


--
-- Name: uniq_4b365660979b1ad6; Type: INDEX; Schema: public; Owner: symfony
--

CREATE UNIQUE INDEX uniq_4b365660979b1ad6 ON public.stock USING btree (company_id);


--
-- Name: uniq_4fbf094f4d4cff2b; Type: INDEX; Schema: public; Owner: symfony
--

CREATE UNIQUE INDEX uniq_4fbf094f4d4cff2b ON public.company USING btree (shipping_address_id);


--
-- Name: uniq_4fbf094f880eceda; Type: INDEX; Schema: public; Owner: symfony
--

CREATE UNIQUE INDEX uniq_4fbf094f880eceda ON public.company USING btree (company_last_vendors_id);


--
-- Name: uniq_80328824979b1ad6; Type: INDEX; Schema: public; Owner: symfony
--

CREATE UNIQUE INDEX uniq_80328824979b1ad6 ON public.company_last_vendors_manager USING btree (company_id);


--
-- Name: uniq_8d93d649e7927c74; Type: INDEX; Schema: public; Owner: symfony
--

CREATE UNIQUE INDEX uniq_8d93d649e7927c74 ON public."user" USING btree (email);


--
-- Name: uniq_eb066945483946e3; Type: INDEX; Schema: public; Owner: symfony
--

CREATE UNIQUE INDEX uniq_eb066945483946e3 ON public.shipping_address USING btree (company_address_id);


--
-- Name: product_in_inventory_done fk_27db1afc4584665a; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_inventory_done
    ADD CONSTRAINT fk_27db1afc4584665a FOREIGN KEY (product_id) REFERENCES public.product(id);


--
-- Name: product_in_inventory_done fk_27db1afce9c97ec4; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_inventory_done
    ADD CONSTRAINT fk_27db1afce9c97ec4 FOREIGN KEY (inventory_done_id) REFERENCES public.inventory_done(id);


--
-- Name: product_sub_category fk_3147d5f3be6903fd; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_sub_category
    ADD CONSTRAINT fk_3147d5f3be6903fd FOREIGN KEY (product_category_id) REFERENCES public.product_category(id);


--
-- Name: product_in_recipe fk_3dfae9f59d8a214; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_recipe
    ADD CONSTRAINT fk_3dfae9f59d8a214 FOREIGN KEY (recipe_id) REFERENCES public.recipe(id);


--
-- Name: product_in_recipe fk_3dfae9f933fe08c; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_recipe
    ADD CONSTRAINT fk_3dfae9f933fe08c FOREIGN KEY (ingredient_id) REFERENCES public.product(id);


--
-- Name: recipe_in_inventory_done fk_4117c00b59d8a214; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.recipe_in_inventory_done
    ADD CONSTRAINT fk_4117c00b59d8a214 FOREIGN KEY (recipe_id) REFERENCES public.recipe(id);


--
-- Name: recipe_in_inventory_done fk_4117c00be9c97ec4; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.recipe_in_inventory_done
    ADD CONSTRAINT fk_4117c00be9c97ec4 FOREIGN KEY (inventory_done_id) REFERENCES public.inventory_done(id);


--
-- Name: stock fk_4b365660979b1ad6; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.stock
    ADD CONSTRAINT fk_4b365660979b1ad6 FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: company fk_4fbf094f4d4cff2b; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT fk_4fbf094f4d4cff2b FOREIGN KEY (shipping_address_id) REFERENCES public.shipping_address(id);


--
-- Name: company fk_4fbf094f880eceda; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT fk_4fbf094f880eceda FOREIGN KEY (company_last_vendors_id) REFERENCES public.company_last_vendors_manager(id);


--
-- Name: inventory_done fk_5a8b2f8552d84f11; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.inventory_done
    ADD CONSTRAINT fk_5a8b2f8552d84f11 FOREIGN KEY (affiliated_stock_id) REFERENCES public.stock(id);


--
-- Name: inventory_done fk_5a8b2f85f675f31b; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.inventory_done
    ADD CONSTRAINT fk_5a8b2f85f675f31b FOREIGN KEY (author_id) REFERENCES public."user"(id);


--
-- Name: product_in_order fk_746ce7713da206a5; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_order
    ADD CONSTRAINT fk_746ce7713da206a5 FOREIGN KEY (order_entity_id) REFERENCES public."order"(id);


--
-- Name: product_in_order fk_746ce7714584665a; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_order
    ADD CONSTRAINT fk_746ce7714584665a FOREIGN KEY (product_id) REFERENCES public.product(id);


--
-- Name: company_last_vendors_manager fk_80328824979b1ad6; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.company_last_vendors_manager
    ADD CONSTRAINT fk_80328824979b1ad6 FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: user fk_8d93d649979b1ad6; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_8d93d649979b1ad6 FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: user fk_8d93d649f603ee73; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_8d93d649f603ee73 FOREIGN KEY (vendor_id) REFERENCES public.vendor(id);


--
-- Name: product_in_stock fk_ca7322894584665a; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_stock
    ADD CONSTRAINT fk_ca7322894584665a FOREIGN KEY (product_id) REFERENCES public.product(id);


--
-- Name: product_in_stock fk_ca732289dcd6110; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product_in_stock
    ADD CONSTRAINT fk_ca732289dcd6110 FOREIGN KEY (stock_id) REFERENCES public.stock(id);


--
-- Name: product fk_d34a04adc534ede1; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT fk_d34a04adc534ede1 FOREIGN KEY (product_sub_category_id) REFERENCES public.product_sub_category(id);


--
-- Name: product fk_d34a04adf603ee73; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT fk_d34a04adf603ee73 FOREIGN KEY (vendor_id) REFERENCES public.vendor(id);


--
-- Name: recipe fk_da88b137979b1ad6; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT fk_da88b137979b1ad6 FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: shipping_address fk_eb066945483946e3; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.shipping_address
    ADD CONSTRAINT fk_eb066945483946e3 FOREIGN KEY (company_address_id) REFERENCES public.company(id);


--
-- Name: company_last_vendors_manager_vendor fk_f3b802926288684f; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.company_last_vendors_manager_vendor
    ADD CONSTRAINT fk_f3b802926288684f FOREIGN KEY (company_last_vendors_manager_id) REFERENCES public.company_last_vendors_manager(id) ON DELETE CASCADE;


--
-- Name: company_last_vendors_manager_vendor fk_f3b80292f603ee73; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.company_last_vendors_manager_vendor
    ADD CONSTRAINT fk_f3b80292f603ee73 FOREIGN KEY (vendor_id) REFERENCES public.vendor(id) ON DELETE CASCADE;


--
-- Name: order fk_f52993982441e783; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT fk_f52993982441e783 FOREIGN KEY (order_company_id) REFERENCES public.company(id);


--
-- PostgreSQL database dump complete
--

