<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220717172507 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE company_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE company_last_vendors_manager_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE inventory_done_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "order_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_category_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_in_inventory_done_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_in_order_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_in_recipe_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_in_stock_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_sub_category_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE recipe_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE recipe_in_inventory_done_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shipping_address_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stock_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE vendor_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE company (id INT NOT NULL, shipping_address_id INT DEFAULT NULL, company_last_vendors_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, siret VARCHAR(14) NOT NULL, company_size INT NOT NULL, email VARCHAR(255) NOT NULL, phone_number VARCHAR(20) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094F4D4CFF2B ON company (shipping_address_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094F880ECEDA ON company (company_last_vendors_id)');
        $this->addSql('CREATE TABLE company_last_vendors_manager (id INT NOT NULL, company_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_80328824979B1AD6 ON company_last_vendors_manager (company_id)');
        $this->addSql('CREATE TABLE company_last_vendors_manager_vendor (company_last_vendors_manager_id INT NOT NULL, vendor_id INT NOT NULL, PRIMARY KEY(company_last_vendors_manager_id, vendor_id))');
        $this->addSql('CREATE INDEX IDX_F3B802926288684F ON company_last_vendors_manager_vendor (company_last_vendors_manager_id)');
        $this->addSql('CREATE INDEX IDX_F3B80292F603EE73 ON company_last_vendors_manager_vendor (vendor_id)');
        $this->addSql('CREATE TABLE inventory_done (id INT NOT NULL, affiliated_stock_id INT NOT NULL, author_id INT NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, note TEXT DEFAULT NULL, revenues INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5A8B2F8552D84F11 ON inventory_done (affiliated_stock_id)');
        $this->addSql('CREATE INDEX IDX_5A8B2F85F675F31B ON inventory_done (author_id)');
        $this->addSql('CREATE TABLE "order" (id INT NOT NULL, order_company_id INT NOT NULL, status VARCHAR(20) NOT NULL, price INT NOT NULL, date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F52993982441E783 ON "order" (order_company_id)');
        $this->addSql('CREATE TABLE product (id INT NOT NULL, vendor_id INT NOT NULL, product_sub_category_id INT DEFAULT NULL, picture VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, weight DOUBLE PRECISION NOT NULL, measuring_unit VARCHAR(10) NOT NULL, price INT NOT NULL, product_name VARCHAR(255) NOT NULL, reference VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D34A04ADF603EE73 ON product (vendor_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADC534EDE1 ON product (product_sub_category_id)');
        $this->addSql('CREATE TABLE product_category (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product_in_inventory_done (id INT NOT NULL, inventory_done_id INT NOT NULL, product_id INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_27DB1AFCE9C97EC4 ON product_in_inventory_done (inventory_done_id)');
        $this->addSql('CREATE INDEX IDX_27DB1AFC4584665A ON product_in_inventory_done (product_id)');
        $this->addSql('CREATE TABLE product_in_order (id INT NOT NULL, order_entity_id INT NOT NULL, product_id INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_746CE7713DA206A5 ON product_in_order (order_entity_id)');
        $this->addSql('CREATE INDEX IDX_746CE7714584665A ON product_in_order (product_id)');
        $this->addSql('CREATE TABLE product_in_recipe (id INT NOT NULL, ingredient_id INT NOT NULL, recipe_id INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3DFAE9F933FE08C ON product_in_recipe (ingredient_id)');
        $this->addSql('CREATE INDEX IDX_3DFAE9F59D8A214 ON product_in_recipe (recipe_id)');
        $this->addSql('CREATE TABLE product_in_stock (id INT NOT NULL, product_id INT NOT NULL, stock_id INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CA7322894584665A ON product_in_stock (product_id)');
        $this->addSql('CREATE INDEX IDX_CA732289DCD6110 ON product_in_stock (stock_id)');
        $this->addSql('CREATE TABLE product_sub_category (id INT NOT NULL, product_category_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3147D5F3BE6903FD ON product_sub_category (product_category_id)');
        $this->addSql('CREATE TABLE recipe (id INT NOT NULL, company_id INT DEFAULT NULL, recipe_name VARCHAR(255) NOT NULL, price INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DA88B137979B1AD6 ON recipe (company_id)');
        $this->addSql('CREATE TABLE recipe_in_inventory_done (id INT NOT NULL, inventory_done_id INT NOT NULL, recipe_id INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4117C00BE9C97EC4 ON recipe_in_inventory_done (inventory_done_id)');
        $this->addSql('CREATE INDEX IDX_4117C00B59D8A214 ON recipe_in_inventory_done (recipe_id)');
        $this->addSql('CREATE TABLE shipping_address (id INT NOT NULL, company_address_id INT DEFAULT NULL, city VARCHAR(60) NOT NULL, address VARCHAR(100) NOT NULL, zipcode VARCHAR(5) NOT NULL, complement_of_address VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EB066945483946E3 ON shipping_address (company_address_id)');
        $this->addSql('CREATE TABLE stock (id INT NOT NULL, company_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4B365660979B1AD6 ON stock (company_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, company_id INT DEFAULT NULL, vendor_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, phone_number VARCHAR(20) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_verified BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE INDEX IDX_8D93D649979B1AD6 ON "user" (company_id)');
        $this->addSql('CREATE INDEX IDX_8D93D649F603EE73 ON "user" (vendor_id)');
        $this->addSql('CREATE TABLE vendor (id INT NOT NULL, name VARCHAR(255) NOT NULL, address VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F4D4CFF2B FOREIGN KEY (shipping_address_id) REFERENCES shipping_address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F880ECEDA FOREIGN KEY (company_last_vendors_id) REFERENCES company_last_vendors_manager (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE company_last_vendors_manager ADD CONSTRAINT FK_80328824979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE company_last_vendors_manager_vendor ADD CONSTRAINT FK_F3B802926288684F FOREIGN KEY (company_last_vendors_manager_id) REFERENCES company_last_vendors_manager (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE company_last_vendors_manager_vendor ADD CONSTRAINT FK_F3B80292F603EE73 FOREIGN KEY (vendor_id) REFERENCES vendor (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE inventory_done ADD CONSTRAINT FK_5A8B2F8552D84F11 FOREIGN KEY (affiliated_stock_id) REFERENCES stock (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE inventory_done ADD CONSTRAINT FK_5A8B2F85F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F52993982441E783 FOREIGN KEY (order_company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF603EE73 FOREIGN KEY (vendor_id) REFERENCES vendor (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADC534EDE1 FOREIGN KEY (product_sub_category_id) REFERENCES product_sub_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_inventory_done ADD CONSTRAINT FK_27DB1AFCE9C97EC4 FOREIGN KEY (inventory_done_id) REFERENCES inventory_done (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_inventory_done ADD CONSTRAINT FK_27DB1AFC4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_order ADD CONSTRAINT FK_746CE7713DA206A5 FOREIGN KEY (order_entity_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_order ADD CONSTRAINT FK_746CE7714584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_recipe ADD CONSTRAINT FK_3DFAE9F933FE08C FOREIGN KEY (ingredient_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_recipe ADD CONSTRAINT FK_3DFAE9F59D8A214 FOREIGN KEY (recipe_id) REFERENCES recipe (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_stock ADD CONSTRAINT FK_CA7322894584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_stock ADD CONSTRAINT FK_CA732289DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_sub_category ADD CONSTRAINT FK_3147D5F3BE6903FD FOREIGN KEY (product_category_id) REFERENCES product_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe ADD CONSTRAINT FK_DA88B137979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_in_inventory_done ADD CONSTRAINT FK_4117C00BE9C97EC4 FOREIGN KEY (inventory_done_id) REFERENCES inventory_done (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipe_in_inventory_done ADD CONSTRAINT FK_4117C00B59D8A214 FOREIGN KEY (recipe_id) REFERENCES recipe (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shipping_address ADD CONSTRAINT FK_EB066945483946E3 FOREIGN KEY (company_address_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B365660979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D649979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D649F603EE73 FOREIGN KEY (vendor_id) REFERENCES vendor (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE company_last_vendors_manager DROP CONSTRAINT FK_80328824979B1AD6');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F52993982441E783');
        $this->addSql('ALTER TABLE recipe DROP CONSTRAINT FK_DA88B137979B1AD6');
        $this->addSql('ALTER TABLE shipping_address DROP CONSTRAINT FK_EB066945483946E3');
        $this->addSql('ALTER TABLE stock DROP CONSTRAINT FK_4B365660979B1AD6');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D649979B1AD6');
        $this->addSql('ALTER TABLE company DROP CONSTRAINT FK_4FBF094F880ECEDA');
        $this->addSql('ALTER TABLE company_last_vendors_manager_vendor DROP CONSTRAINT FK_F3B802926288684F');
        $this->addSql('ALTER TABLE product_in_inventory_done DROP CONSTRAINT FK_27DB1AFCE9C97EC4');
        $this->addSql('ALTER TABLE recipe_in_inventory_done DROP CONSTRAINT FK_4117C00BE9C97EC4');
        $this->addSql('ALTER TABLE product_in_order DROP CONSTRAINT FK_746CE7713DA206A5');
        $this->addSql('ALTER TABLE product_in_inventory_done DROP CONSTRAINT FK_27DB1AFC4584665A');
        $this->addSql('ALTER TABLE product_in_order DROP CONSTRAINT FK_746CE7714584665A');
        $this->addSql('ALTER TABLE product_in_recipe DROP CONSTRAINT FK_3DFAE9F933FE08C');
        $this->addSql('ALTER TABLE product_in_stock DROP CONSTRAINT FK_CA7322894584665A');
        $this->addSql('ALTER TABLE product_sub_category DROP CONSTRAINT FK_3147D5F3BE6903FD');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04ADC534EDE1');
        $this->addSql('ALTER TABLE product_in_recipe DROP CONSTRAINT FK_3DFAE9F59D8A214');
        $this->addSql('ALTER TABLE recipe_in_inventory_done DROP CONSTRAINT FK_4117C00B59D8A214');
        $this->addSql('ALTER TABLE company DROP CONSTRAINT FK_4FBF094F4D4CFF2B');
        $this->addSql('ALTER TABLE inventory_done DROP CONSTRAINT FK_5A8B2F8552D84F11');
        $this->addSql('ALTER TABLE product_in_stock DROP CONSTRAINT FK_CA732289DCD6110');
        $this->addSql('ALTER TABLE inventory_done DROP CONSTRAINT FK_5A8B2F85F675F31B');
        $this->addSql('ALTER TABLE company_last_vendors_manager_vendor DROP CONSTRAINT FK_F3B80292F603EE73');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04ADF603EE73');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D649F603EE73');
        $this->addSql('DROP SEQUENCE company_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE company_last_vendors_manager_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE inventory_done_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "order_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE product_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_category_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_in_inventory_done_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_in_order_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_in_recipe_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_in_stock_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_sub_category_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE recipe_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE recipe_in_inventory_done_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shipping_address_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stock_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE vendor_id_seq CASCADE');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE company_last_vendors_manager');
        $this->addSql('DROP TABLE company_last_vendors_manager_vendor');
        $this->addSql('DROP TABLE inventory_done');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_category');
        $this->addSql('DROP TABLE product_in_inventory_done');
        $this->addSql('DROP TABLE product_in_order');
        $this->addSql('DROP TABLE product_in_recipe');
        $this->addSql('DROP TABLE product_in_stock');
        $this->addSql('DROP TABLE product_sub_category');
        $this->addSql('DROP TABLE recipe');
        $this->addSql('DROP TABLE recipe_in_inventory_done');
        $this->addSql('DROP TABLE shipping_address');
        $this->addSql('DROP TABLE stock');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE vendor');
    }
}
