## Getting Started

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/)
2. Run `docker-compose build --pull --no-cache` to build fresh images
3. Get the .env.local.demo file and rename it as .env.local
4. Run `docker-compose exec php sh -c '
   set -e
   apk add openssl
   php bin/console lexik:jwt:generate-keypair
   setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
   setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt'` to generate JWT keypair
5. Run `docker-compose up` (the logs will be displayed in the current shell)
6. Open `https://localhost` in your favorite web browser and [accept the auto-generated TLS certificate](https://stackoverflow.com/a/15076602/1352334)
7. Run `docker-compose down --remove-orphans` to stop the Docker containers.

## Makefile

You can use make for the Makefile (see the Makefile if you want to see what is available)

## Credits for the docker configuration

Created by [Kévin Dunglas](https://dunglas.fr), co-maintained by [Maxime Helias](https://twitter.com/maxhelias) and sponsored by [Les-Tilleuls.coop](https://les-tilleuls.coop).
set

## Credit for the current project

- Alexandre Robert
- Jérémy Delbart
